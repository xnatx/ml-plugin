/*
 * ml-plugin: org.nrg.xnatx.plugins.ml.services.impl.DefaultMlConfigurationServiceTests
 * XNAT http://www.xnat.org
 * Copyright (c) 2005-2021, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.xnatx.plugins.ml.services.impl;

import static java.util.stream.Collectors.toList;
import static org.assertj.core.api.Assertions.assertThat;

import com.fasterxml.jackson.databind.JsonNode;
import com.google.common.collect.ImmutableMap;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.apache.commons.io.IOUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.nrg.framework.configuration.SerializerConfig;
import org.nrg.framework.services.SerializerService;
import org.nrg.framework.utilities.BasicXnatResourceLocator;
import org.nrg.xdat.om.base.BaseMlParameterizedconfig;
import org.nrg.xnatx.plugins.ml.utils.ValueConvertingParameterMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = SerializerConfig.class)
public class DefaultMlConfigurationServiceTests {
    @Test
    public void testParameterizedSerialization() throws IOException {
        final String      template       = IOUtils.toString(BasicXnatResourceLocator.getResource("annotation_ct_liver.json").getInputStream(), StandardCharsets.UTF_8);
        final Set<String> parameterPaths = PARAMETERS.keySet();
        final JsonNode    parameterized  = BaseMlParameterizedconfig.renderImpl(_serializer, template, parameterPaths, PARAMETERS);
        final JsonNode    reference      = _serializer.deserializeJson(IOUtils.toString(BasicXnatResourceLocator.getResource("annotation_ct_liver_reference.json").getInputStream(), StandardCharsets.UTF_8));
        assertThat(parameterized).isEqualTo(reference);
    }

    @Test
    public void testConvertValue() {
        final ValueConvertingParameterMap map = new ValueConvertingParameterMap();
        VALUES.forEach(value -> map.put(value, value));
        final List<Integer> types = VALUES.stream().map(map::getConvertedValue).map(DefaultMlConfigurationServiceTests::getType).collect(toList());
        assertThat(types).isEqualTo(TYPES);
    }

    private static int getType(final Object value) {
        if (value instanceof Integer) {
            return 0;
        }
        if (value instanceof Double) {
            return 1;
        }
        if (value instanceof Boolean) {
            return 2;
        }
        return 3;
    }

    private static final List<String>        PARAMETERIZABLE = Arrays.asList("epochs", "multi_gpu", "num_training_epoch_per_valid", "learning_rate");
    private static final Map<String, String> PARAMETERS      = ImmutableMap.of("epochs", "500", "multi_gpu", "true", "num_training_epoch_per_valid", "20", "learning_rate", "1e-4");
    private static final List<String>        VALUES          = Arrays.asList("0", "0.0", "{PRETRAIN_WEIGHTS_FILE}", "0.1", "0.10", "0.2", "0.20", "0.6", "0.7", "SegmAhnet3D", "0.9", "1", "1.0", "10", "false", "1000", "10000", "1024", "TRUE", "1250", "1300", "16", "164", "FALSE", "175", "189", "Adam", "199", "1e-3", "1e-4", "Dice", "1e-5", "2", "20", "2000", "2048", "3", "300", "4", "true", "5000", "50000", "NPRandomFlip3D", "5e-4", "60", "8", "8e-4", "9000");
    private static final List<Integer>       TYPES           = Arrays.asList(0, 1, 3, 1, 1, 1, 1, 1, 1, 3, 1, 0, 1, 0, 2, 0, 0, 0, 2, 0, 0, 0, 0, 2, 0, 0, 3, 0, 1, 1, 3, 1, 0, 0, 0, 0, 0, 0, 0, 2, 0, 0, 3, 1, 0, 0, 1, 0);

    @Autowired
    private SerializerService _serializer;
}
