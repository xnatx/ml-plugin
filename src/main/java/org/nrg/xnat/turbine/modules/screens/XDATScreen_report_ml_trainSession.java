/*
 * ml-plugin: org.nrg.xnat.turbine.modules.screens.XDATScreen_report_ml_trainSession
 * XNAT http://www.xnat.org
 * Copyright (c) 2005-2021, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.xnat.turbine.modules.screens;

import java.io.FileNotFoundException;
import lombok.extern.slf4j.Slf4j;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;
import org.nrg.xdat.om.MlTrainsession;
import org.nrg.xdat.turbine.modules.screens.SecureReport;

@SuppressWarnings("unused")
@Slf4j
public class XDATScreen_report_ml_trainSession extends SecureReport {
    @Override
    public void finalProcessing(final RunData data, final Context context) {
        final MlTrainsession om = new MlTrainsession(item);
        context.put("om", om);
        log.debug("Loaded MlTrainsession with ID {} as context parameter 'om'.", om.getId());
        setDefaultTabs(DEFAULT_TABS);
        try {
            cacheTabs(context, XNAT_ML_TRAINSESSION_TABS);
        } catch (FileNotFoundException e) {
            log.warn("An error occurred trying to cache tabs from the folder {}", XNAT_ML_TRAINSESSION_TABS);
        }
    }

    private static final String XNAT_ML_TRAINSESSION_TABS = "ml_trainSession/tabs";
    private static final String DEFAULT_TABS              = "ml_trainSession_summary_details";
}
