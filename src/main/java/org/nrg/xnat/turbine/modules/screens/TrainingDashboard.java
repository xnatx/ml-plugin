/*
 * ml-plugin: org.nrg.xnat.turbine.modules.screens.TrainingDashboard
 * XNAT http://www.xnat.org
 * Copyright (c) 2005-2021, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.xnat.turbine.modules.screens;

import lombok.extern.slf4j.Slf4j;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;
import org.nrg.xdat.turbine.modules.screens.SecureReport;

@SuppressWarnings("unused")
@Slf4j
public class TrainingDashboard extends SecureReport {
    @Override
    public void preProcessing(final RunData data, final Context context) {
        log.info("Now in TrainingDashboard.preProcessing()");
    }

    @Override
    public void finalProcessing(final RunData data, final Context context) {
        log.info("Now in TrainingDashboard.finalProcessing()");
        try {
            final String projectId = (String) om.getProperty("id");
            context.put("project", om);
            context.put("projectId", projectId);
        } catch (Exception e) {
            log.error("An error occurred trying to get the ID property from the om of type: {}", om.getClass().getName());
        }
    }
}
