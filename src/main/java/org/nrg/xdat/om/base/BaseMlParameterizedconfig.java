/*
 * ml-plugin: org.nrg.xdat.om.base.BaseMlParameterizedconfig
 * XNAT http://www.xnat.org
 * Copyright (c) 2005-2021, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.xdat.om.base;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;
import com.jayway.jsonpath.PathNotFoundException;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.experimental.Accessors;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.nrg.framework.services.SerializerService;
import org.nrg.xdat.XDAT;
import org.nrg.xdat.om.base.auto.AutoMlParameterizedconfig;
import org.nrg.xft.ItemI;
import org.nrg.xft.security.UserI;
import org.nrg.xnatx.plugins.ml.exceptions.XnatMlException;
import org.nrg.xnatx.plugins.ml.preferences.MlResources;
import org.nrg.xnatx.plugins.ml.utils.ValueConvertingParameterMap;

import java.io.IOException;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

/**
 * Override of generated implementation of this class to provide configuration rendering.
 **/
@Getter(AccessLevel.PROTECTED)
@Accessors(prefix = "_")
@Slf4j
public abstract class BaseMlParameterizedconfig extends AutoMlParameterizedconfig {
    public BaseMlParameterizedconfig(final ItemI item) {
        super(item);
        _serializer = XDAT.getContextService().getBean(SerializerService.class);
        _resources = XDAT.getContextService().getBean(MlResources.class);
    }

    public BaseMlParameterizedconfig(final UserI user) {
        super(user);
        _serializer = XDAT.getContextService().getBean(SerializerService.class);
        _resources = XDAT.getContextService().getBean(MlResources.class);
    }

    /**
     * @deprecated Use {@link #BaseMlParameterizedconfig(UserI)}.
     */
    public BaseMlParameterizedconfig() {
        _serializer = XDAT.getContextService().getBean(SerializerService.class);
        _resources = XDAT.getContextService().getBean(MlResources.class);
    }

    public BaseMlParameterizedconfig(final Hashtable properties, final UserI user) {
        super(properties, user);
        _serializer = XDAT.getContextService().getBean(SerializerService.class);
        _resources = XDAT.getContextService().getBean(MlResources.class);
    }

    @Override
    public String getParameterizable() {
        return super.getParameterizable();
    }

    @Override
    public void setParameterizable(final String parameterizable) {
        if (StringUtils.startsWith(parameterizable, "{")) {
            setParameterPaths(Collections.emptyList());
        } else {
            setParameterPaths(convertToPaths(parameterizable));
        }
        super.setParameterizable(parameterizable);
    }

    public JsonNode getJsonTemplate() {
        try {
            return StringUtils.isNotBlank(getTemplate()) ? getSerializer().deserializeJson(getTemplate()) : null;
        } catch (IOException e) {
            throw new XnatMlException(e);
        }
    }

    @SuppressWarnings("unused")
    public void setJsonTemplate(final JsonNode node) {
        try {
            setTemplate(getSerializer().toJson(node));
        } catch (IOException e) {
            throw new XnatMlException(e);
        }
    }

    public Collection<String> getParameterPaths() {
        return convertToPaths(getParameterizable());
    }

    public void setParameterPaths(final Collection<String> paths) {
        super.setParameterizable(convertToParameterizable(paths));
    }

    public Map<String, Map<String, String>> getParameterValues() {
        return getParameterPaths().stream().collect(Collectors.toMap(Function.identity(), this::getMap));
    }

    public Map<String, String> getParameterMap() {
        try {
            return getSerializer().deserializeJsonToMapOfStrings(getParameterizable());
        } catch (IOException e) {
            log.error("An error occurred trying to deserialize the value: {}", getParameterizable(), e);
            return Collections.emptyMap();
        }
    }

    @SuppressWarnings("unused")
    public JsonNode getJsonParameterizable() {
        try {
            return StringUtils.isNotBlank(getParameterizable()) ? getSerializer().deserializeJson(getParameterizable()) : null;
        } catch (IOException e) {
            throw new XnatMlException(e, "The parameterizable value doesn't seem to be deserializable to JSON: " + getParameterizable());
        }
    }

    @SuppressWarnings("unused")
    public void setJsonParameterizable(final JsonNode node) {
        try {
            setParameterizable(getSerializer().toJson(node));
        } catch (IOException e) {
            throw new XnatMlException(e);
        }
    }

    public JsonNode render(final Map<String, String> parameters) throws IOException {
        return renderImpl(getSerializer(), getTemplate(), getParameterPaths(), parameters);
    }

    public void setFromJson(final JsonNode node) {
        final boolean hasTemplate        = node.has("template");
        final boolean hasParameterizable = node.has("parameterizable");
        final String  template, parameterizable;
        if (!hasTemplate && !hasParameterizable) {
            template = node.toString();
            parameterizable = null;
        } else {
            if (!hasTemplate) {
                throw new XnatMlException("You must specify the template for parameterized configurations!");
            }
            final JsonNode templateNode = node.get("template");
            template = templateNode.isObject() ? templateNode.toString() : templateNode.textValue();
            if (hasParameterizable) {
                final JsonNode parameterizableNode = node.get("parameterizable");
                parameterizable = parameterizableNode.isArray() ? StreamSupport.stream(parameterizableNode.spliterator(), false).map(JsonNode::textValue).collect(Collectors.joining(", ")) : parameterizableNode.toString();
            } else {
                parameterizable = null;
            }
        }
        log.info("Got the value for template: {}", template);
        setTemplate(template);
        if (hasParameterizable) {
            log.info("Got the value for parameterizable: {}", parameterizable);
            setParameterizable(parameterizable);
        } else {
            log.info("Template set without parameterizable options.");
        }
    }

    /**
     * This method is pulled out as a static to allow for proper unit testing of the render functionality. This method should <i>not</i>
     * be called when rendering a parameterized config in normal practice. Call the object's {@link #render(Map)} method instead.
     *
     * @param serializer     The serializer instance
     * @param template       The {@link #getTemplate() configuration template}
     * @param parameterPaths The {@link #getParameterPaths() allowed parameterizable paths}
     * @param parameters     The parameters and values for rendering the template
     *
     * @return The rendered configuration
     *
     * @throws IOException When an error occurs processing (reading or writing) JSON
     */
    public static JsonNode renderImpl(final SerializerService serializer, final String template, final Collection<String> parameterPaths, final Map<String, String> parameters) throws IOException {
        if (parameters.isEmpty()) {
            return serializer.deserializeJson(template);
        }
        if (!parameterPaths.containsAll(parameters.keySet())) {
            throw new XnatMlException("Tried to set the value for a JSON path that's not configured as parameterizable: " + parameters.keySet().stream().filter(parameter -> !parameterPaths.contains(parameter)).collect(Collectors.joining(", ")));
        }
        final DocumentContext             context      = JsonPath.parse(template);
        final ValueConvertingParameterMap parameterMap = new ValueConvertingParameterMap(parameters);
        for (final String key : parameters.keySet()) {
            final JsonPath path = JsonPath.compile(key);
            try {
                final Object currentValue = context.read(path);
                final Object renderValue  = parameterMap.getConvertedValue(key);
                // TODO: Set up validation to confirm that current and render values are compatible types, should be set, etc.
                log.debug("Setting path {}:\n\n * Current value: {}\n\nRender value: {} ({})", key, currentValue, renderValue, renderValue.getClass().getName());
                context.set(path, renderValue);
            } catch (PathNotFoundException e) {
                throw new PathNotFoundException(key, e);
            }
        }
        try {
            return serializer.deserializeJson(context.jsonString());
        } catch (IOException e) {
            throw new IOException(context.jsonString(), e);
        }
    }

    private Map<String, String> getMap(final String path) {
        final Map<String, String> values = new HashMap<>();
        values.put("display", _resources.getMessage("training.attributes." + path));
        values.put("default", StringUtils.unwrap(getSerializer().getObjectMapper().convertValue(getDocumentContext().read(path), JsonNode.class).toString(), '"'));
        return values;
    }

    private DocumentContext getDocumentContext() {
        return JsonPath.parse(getTemplate());
    }

    private List<String> convertToPaths(final String parameterizable) {
        final String work = StringUtils.startsWith(parameterizable, "[") && StringUtils.endsWith(parameterizable, "]") ? parameterizable : "[" + parameterizable + "]";
        try {
            return _serializer.deserializeJson(work, STRING_LIST_REFERENCE);
        } catch (IOException e) {
            log.error("An error occurred trying to convert a value into a list of strings: \"{}\"", parameterizable, e);
            return Collections.emptyList();
        }
    }

    private static String convertToParameterizable(final Collection<String> paths) {
        return StringUtils.join(paths, ", ");
    }

    private static final TypeReference<ArrayList<String>> STRING_LIST_REFERENCE = new TypeReference<ArrayList<String>>() {};

    private final SerializerService _serializer;
    private final MlResources       _resources;
}
