/*
 * ml-plugin: org.nrg.xdat.om.base.XnatMlResourceHelper
 * XNAT http://www.xnat.org
 * Copyright (c) 2005-2021, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.xdat.om.base;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.nrg.xdat.XDAT;
import org.nrg.xdat.model.XnatAbstractresourceI;
import org.nrg.xdat.model.XnatExperimentdataI;
import org.nrg.xdat.om.XnatExperimentdata;
import org.nrg.xdat.om.XnatResourcecatalog;
import org.nrg.xdat.security.helpers.Permissions;
import org.nrg.xft.security.UserI;
import org.nrg.xnat.helpers.resource.XnatResourceInfoMap;
import org.nrg.xnat.helpers.uri.UriParserUtils;
import org.nrg.xnat.services.archive.CatalogService;
import org.nrg.xnatx.plugins.ml.exceptions.XnatMlException;
import org.springframework.core.io.InputStreamSource;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.nio.file.Path;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.function.Supplier;

@Slf4j
public class MlResourceHelper<T extends XnatExperimentdataI> {
    public MlResourceHelper(final T experiment) {
        _catalogService = XDAT.getContextService().getBean(CatalogService.class);
        _experiment = experiment;
    }

    /**
     * Gets the resource catalog for this object. If the catalog has not yet been created, a new
     * catalog is created (<b>user</b> must have sufficient permissions) and returned.
     *
     * @return The specified resource catalog for this object.
     */
    @Nullable
    public XnatResourcecatalog getXnatMlResource(final UserI user, final String name) {
        try {
            if (!Permissions.canRead(user, ((XnatExperimentdata) _experiment))) {
                return null;
            }
        } catch (Exception e) {
            throw new XnatMlException(e);
        }
        return getResources().stream().parallel()
                             .filter(resource -> StringUtils.equals(XnatResourcecatalog.SCHEMA_ELEMENT_NAME, resource.getXSIType())
                                                 && StringUtils.equals(name, resource.getLabel()))
                             .map(XnatResourcecatalog.class::cast)
                             .findFirst()
                             .orElse(null);
    }

    /**
     * Gets the resource catalog for this object. If the catalog has not yet been created, a new
     * catalog is created (<b>user</b> must have sufficient permissions) and returned.
     *
     * @return The resource catalog for this object.
     */
    public XnatResourcecatalog getXnatMlResource(final UserI user, final String name, final String format, final String content) {
        if (StringUtils.isAnyBlank(name, format)) {
            throw new XnatMlException("You must provide values for at least name and format (content is optional)");
        }
        return getResources().stream().parallel()
                             .filter(resource -> StringUtils.equals(XnatResourcecatalog.SCHEMA_ELEMENT_NAME, resource.getXSIType())
                                                 && StringUtils.equals(name, resource.getLabel())
                                                 && StringUtils.equals(format, ((XnatResourcecatalog) resource).getFormat()))
                             .map(XnatResourcecatalog.class::cast)
                             .findFirst()
                             .orElseGet(resourceSupplier(user, name, format, content));
    }

    public Path getXnatMlResourceData() {
        // TODO: Rig up to return object resource data.
        return null;
    }

    /**
     * Creates the resource catalog for the current experiment.
     *
     * @param user    The user adding data to the resource.
     * @param name    The name of the resource to be set.
     * @param format  The format of the resource to be set.
     * @param content The content of the resource to be set.
     *
     * @return The created or updated resource catalog.
     *
     * @throws XnatMlException When an error occurs.
     */
    public XnatResourcecatalog createXnatMlResource(final UserI user, final String name, final String format, final String content) throws XnatMlException {
        return setXnatMlResourceData(user, name, format, content, Collections.emptyMap());
    }

    /**
     * Adds the file or files indicated by the <b>paths</b> parameter to the resource catalog.
     *
     * @param user       The user adding data to the resource.
     * @param name       The name of the resource to be set.
     * @param format     The format of the resource to be set.
     * @param content    The content of the resource to be set.
     * @param references One or more URIs indicating the data to add to the resource.
     *
     * @throws XnatMlException When an error occurs.
     */
    public XnatResourcecatalog setXnatMlResourceData(final UserI user, final String name, final String format, final String content, final @Nonnull Map<String, ? extends InputStreamSource> references) throws XnatMlException {
        final XnatResourcecatalog resource = getXnatMlResource(user, name, format, content);
        if (resource == null) {
            if (user != null) {
                throw new XnatMlException(user, "Could not retrieve or create a resource for the data object " + getLabel() + " in project " + getProject());
            } else {
                throw new XnatMlException("Could not retrieve or create a resource for the data object " + getLabel() + " in project " + getProject());
            }
        }
        log.info("Got catalog {} on experiment {} for resource {}, format {}, content{}, for user {}", resource.getUri(), _experiment.getId(), name, format, content, user.getUsername());
        try {
            final XnatResourcecatalog inserted;
            if (references.isEmpty()) {
                final String archiveUri = UriParserUtils.getArchiveUri(_experiment);
                inserted = _catalogService.insertResourceCatalog(user, archiveUri, resource);
                log.info("User {} inserted resource catalog {} for experiment {} at URI {}", user.getUsername(), inserted.getUri(), _experiment.getId(), archiveUri);
            } else {
                XnatResourceInfoMap xnatResourceInfoMap = new XnatResourceInfoMap(references);
                xnatResourceInfoMap.setDefaultExtractCompressedResource(true);
                inserted = _catalogService.insertResourceStreams(user, resource, xnatResourceInfoMap);
                log.info("User {} inserted resource catalog {} for experiment {} with resources: {}", user.getUsername(), inserted.getUri(), _experiment.getId(), StringUtils.join(references.keySet(), ", "));
            }
            return inserted;
        } catch (XnatMlException e) {
            throw e;
        } catch (Exception e) {
            throw new XnatMlException(user, e, "Got an exception trying to insert resources into the data object resource " + resource.getLabel() + " in project " + getProject());
        }
    }

    public List<XnatAbstractresourceI> getResources() {
        return _experiment.getResources_resource();
    }

    private String getLabel() {
        return _experiment.getLabel();
    }

    private String getProject() {
        return _experiment.getProject();
    }

    private Supplier<XnatResourcecatalog> resourceSupplier(final UserI user, final String name, final String format, final String content) {
        return () -> createCatalog(user, name, format, content);
    }

    /**
     * This method exists just to wrap the exception handling away from the lambda in {@link #resourceSupplier(UserI, String, String, String)}.
     *
     * @param user    The user creating the catalog.
     * @param name    The name for the resource catalog.
     * @param format  The format for the resource catalog.
     * @param content The content for the resource catalog.
     *
     * @return A new resource catalog for the specified object.
     */
    @Nullable
    private XnatResourcecatalog createCatalog(final @Nullable UserI user, final String name, final String format, final @Nullable String content) {
        if (user == null) {
            return null;
        }

        final String uri = UriParserUtils.getArchiveUri(_experiment);
        try {
            return _catalogService.createAndInsertResourceCatalog(user, uri, null, name, String.format(CREATED_RESOURCE_CATALOG, name, getLabel(), getProject()), format, content);
        } catch (Exception e) {
            throw new XnatMlException(user, e, "An error occurred trying to create a catalog for archive location " + uri);
        }
    }

    private static final String CREATED_RESOURCE_CATALOG = "Creating resource catalog \"%s\" for data object \"%s\" in project %s";

    private final CatalogService _catalogService;
    private final T              _experiment;
}
