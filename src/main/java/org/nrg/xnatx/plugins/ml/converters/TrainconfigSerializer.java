/*
 * ml-plugin: org.nrg.xnatx.plugins.ml.converters.TrainconfigSerializer
 * XNAT http://www.xnat.org
 * Copyright (c) 2005-2021, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.xnatx.plugins.ml.converters;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import java.io.IOException;
import lombok.extern.slf4j.Slf4j;
import org.nrg.xdat.om.MlTrainconfig;
import org.nrg.xnatx.plugins.datasets.converters.DatasetSerializer;

@Slf4j
public class TrainconfigSerializer extends DatasetSerializer<MlTrainconfig> {
    public TrainconfigSerializer() {
        super(MlTrainconfig.class);
    }

    @Override
    public void serialize(final MlTrainconfig trainConfig, final JsonGenerator generator, final SerializerProvider serializer) throws IOException {
        generator.writeStartObject();
        writeNonBlankField(generator, "id", trainConfig.getId());
        writeNonBlankField(generator, "label", trainConfig.getLabel());
        writeNonBlankField(generator, "description", trainConfig.getDescription());
        writeNonBlankField(generator, "project", trainConfig.getProject());
        writeNonBlankField(generator, "type", trainConfig.getType());
        generator.writeObjectField("configuration", trainConfig.getConfiguration());
        generator.writeObjectField("dataset", trainConfig.getDataset());
        writeMetadata(generator, trainConfig);
        generator.writeEndObject();
    }
}
