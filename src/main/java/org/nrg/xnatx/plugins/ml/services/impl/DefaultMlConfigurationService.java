/*
 * ml-plugin: org.nrg.xnatx.plugins.ml.services.impl.DefaultMlConfigurationService
 * XNAT http://www.xnat.org
 * Copyright (c) 2005-2021, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.xnatx.plugins.ml.services.impl;

import static lombok.AccessLevel.PRIVATE;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.gs.collections.impl.factory.Sets;
import com.jayway.jsonpath.PathNotFoundException;
import java.io.IOException;
import java.util.AbstractMap;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import lombok.Getter;
import lombok.experimental.Accessors;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.nrg.framework.services.SerializerService;
import org.nrg.xapi.exceptions.DataFormatException;
import org.nrg.xapi.exceptions.InsufficientPrivilegesException;
import org.nrg.xapi.exceptions.NotFoundException;
import org.nrg.xapi.exceptions.ResourceAlreadyExistsException;
import org.nrg.xdat.om.MlParameterizedconfig;
import org.nrg.xdat.om.MlTrainconfig;
import org.nrg.xdat.om.SetsCollection;
import org.nrg.xdat.security.SecurityManager;
import org.nrg.xdat.security.helpers.Permissions;
import org.nrg.xft.security.UserI;
import org.nrg.xnatx.plugins.ml.exceptions.XnatMlException;
import org.nrg.xnatx.plugins.ml.preferences.MlResources;
import org.nrg.xnatx.plugins.ml.services.MlConfigurationService;
import org.nrg.xnatx.plugins.datasets.services.DatasetCollectionService;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Service;

// TODO: Migrate to extend AbstractXftDatasetObjectService instead of AbstractManagementService
/**
 * Manages XNAT ML data objects and operations.
 */
@Service
@Getter(PRIVATE)
@Accessors(prefix = "_")
@Slf4j
public class DefaultMlConfigurationService extends AbstractManagementService<MlTrainconfig> implements MlConfigurationService {
    public DefaultMlConfigurationService(final DatasetCollectionService collections, final SerializerService serializer, final NamedParameterJdbcTemplate template, final MlResources resources) {
        super(template, resources);
        _collections = collections;
        _serializer = serializer;
    }

    /**
     * {@inheritDoc}
     */
    @Nonnull
    @Override
    public MlTrainconfig createTrainingConfiguration(final UserI user, final MlTrainconfig configuration) throws ResourceAlreadyExistsException, DataFormatException {
        return createXnatMlObject(user, configuration);
    }

    /**
     * {@inheritDoc}
     */
    @Nonnull
    @Override
    public MlTrainconfig createTrainingConfiguration(final UserI user, final String project, final String label, final String template, final List<String> parameterized) throws ResourceAlreadyExistsException, DataFormatException {
        final MlTrainconfig trainconfig = new MlTrainconfig();
        trainconfig.setId(getNewExperimentId());
        trainconfig.setUser(user);
        trainconfig.setProject(project);
        trainconfig.setLabel(label);
        trainconfig.setConfiguration(template, parameterized);
        return createTrainingConfiguration(user, trainconfig);
    }

    /**
     * {@inheritDoc}
     */
    @Nonnull
    @Override
    public MlTrainconfig createTrainingConfiguration(final UserI user, final String project, final String label, final String configuration, final String... parameterized) throws ResourceAlreadyExistsException, DataFormatException {
        return createTrainingConfiguration(user, project, label, configuration, Arrays.asList(parameterized));
    }

    /**
     * {@inheritDoc}
     */
    @Nonnull
    @Override
    public MlTrainconfig createTrainingConfiguration(final UserI user, final String project, final String label, final JsonNode configuration, final List<String> parameterized) throws ResourceAlreadyExistsException, DataFormatException {
        return createTrainingConfiguration(user, project, label, configuration.toString(), parameterized);
    }

    /**
     * {@inheritDoc}
     */
    @Nonnull
    @Override
    public MlTrainconfig createTrainingConfiguration(final UserI user, final String project, final String label, final JsonNode configuration, final String... parameterized) throws ResourceAlreadyExistsException, DataFormatException {
        return createTrainingConfiguration(user, project, label, configuration.toString(), Arrays.asList(parameterized));
    }

    /**
     * {@inheritDoc}
     */
    @Nonnull
    @Override
    public Map<String, Map<String, String>> retrieveTrainingConfigurations(final UserI user) {
        return Permissions.getAllAccessibleExperimentsOfType(getTemplate(), user, MlTrainconfig.SCHEMA_ELEMENT_NAME, SecurityManager.READ);
    }

    /**
     * {@inheritDoc}
     */
    @Nonnull
    @Override
    public Map<String, String> retrieveTrainingConfigurations(final UserI user, final String project) throws NotFoundException, InsufficientPrivilegesException {
        return getObjectsInProject(user, project);
    }

    @Nonnull
    @Override
    public MlTrainconfig retrieveTrainingConfiguration(final UserI user, final String id) throws NotFoundException {
        return getExperiment(user, id);
    }

    /**
     * {@inheritDoc}
     */
    @Nonnull
    @Override
    public MlTrainconfig retrieveTrainingConfiguration(@Nonnull final UserI user, @Nullable final String project, @Nonnull final String idOrLabel) throws NotFoundException {
        return getExperiment(user, project, idOrLabel);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Nonnull
    public MlTrainconfig updateTrainingConfiguration(final UserI user, final MlTrainconfig configuration) throws DataFormatException {
        return saveXnatMlObject(user, configuration);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Nonnull
    public MlTrainconfig updateTrainingConfiguration(final UserI user, final String id, final String configuration, final List<String> parameterized) throws NotFoundException, DataFormatException {
        final MlTrainconfig trainconfig = retrieveTrainingConfiguration(user, id);
        if (StringUtils.isNotBlank(configuration)) {
            trainconfig.setConfiguration(configuration, parameterized);
        }
        return updateTrainingConfiguration(user, trainconfig);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Nonnull
    public MlTrainconfig updateTrainingConfiguration(final UserI user, final String id, final String configuration, final String... parameterized) throws NotFoundException, DataFormatException {
        return updateTrainingConfiguration(user, id, configuration, Arrays.asList(parameterized));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Nonnull
    public MlTrainconfig updateTrainingConfiguration(final UserI user, final String id, final JsonNode configuration, final List<String> parameterized) throws NotFoundException, DataFormatException {
        return updateTrainingConfiguration(user, id, configuration.toString(), parameterized);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Nonnull
    public MlTrainconfig updateTrainingConfiguration(final UserI user, final String id, final JsonNode configuration, final String... parameterized) throws NotFoundException, DataFormatException {
        return updateTrainingConfiguration(user, id, configuration.toString(), Arrays.asList(parameterized));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Nonnull
    public MlTrainconfig updateTrainingConfiguration(final UserI user, final String project, final String idOrLabel, final String configuration, final List<String> parameterized) throws NotFoundException, DataFormatException {
        return updateTrainingConfiguration(user, resolveIdOrLabelInProject(project, idOrLabel), configuration, parameterized);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Nonnull
    public MlTrainconfig updateTrainingConfiguration(final UserI user, final String project, final String idOrLabel, final String configuration, final String... parameterized) throws NotFoundException, DataFormatException {
        return updateTrainingConfiguration(user, resolveIdOrLabelInProject(project, idOrLabel), configuration, Arrays.asList(parameterized));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Nonnull
    public MlTrainconfig updateTrainingConfiguration(final UserI user, final String project, final String idOrLabel, final JsonNode configuration, final List<String> parameterized) throws NotFoundException, DataFormatException {
        return updateTrainingConfiguration(user, resolveIdOrLabelInProject(project, idOrLabel), configuration.toString(), parameterized);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Nonnull
    public MlTrainconfig updateTrainingConfiguration(final UserI user, final String project, final String idOrLabel, final JsonNode configuration, final String... parameterized) throws NotFoundException, DataFormatException {
        return updateTrainingConfiguration(user, resolveIdOrLabelInProject(project, idOrLabel), configuration.toString(), Arrays.asList(parameterized));
    }

    @Nonnull
    @Override
    public MlTrainconfig updateTrainingConfigurationParameters(final UserI user, final String id, final List<String> parameterized) throws NotFoundException, DataFormatException {
        return updateTrainingConfiguration(user, id, (String) null, parameterized);
    }

    @Nonnull
    @Override
    public MlTrainconfig updateTrainingConfigurationParameters(final UserI user, final String project, final String idOrLabel, final List<String> parameterized) throws NotFoundException, DataFormatException {
        return updateTrainingConfiguration(user, project, idOrLabel, (String) null, parameterized);
    }

    /**
     * {@inheritDoc}
     */
    @Nonnull
    @Override
    public JsonNode renderTrainingConfiguration(final UserI user, final String id, final Map<String, String> parameters) throws NotFoundException {
        return renderTrainingConfiguration(user, retrieveTrainingConfiguration(user, id), parameters);
    }

    /**
     * {@inheritDoc}
     */
    @Nonnull
    @Override
    public JsonNode renderTrainingConfiguration(final UserI user, final String project, final String idOrLabel, final Map<String, String> parameters) throws NotFoundException {
        return renderTrainingConfiguration(user, resolveIdOrLabelInProject(project, idOrLabel), parameters);
    }

    /**
     * {@inheritDoc}
     */
    @Nonnull
    @Override
    public JsonNode renderTrainingConfiguration(final UserI user, final MlTrainconfig trainConfig, final Map<String, String> parameters) {
        if (parameters.isEmpty()) {
            return trainConfig.getConfiguration().getJsonTemplate();
        }
        final Collection<String> parameterPaths = trainConfig.getConfiguration().getParameterPaths();
        if (!parameterPaths.containsAll(parameters.keySet())) {
            throw new XnatMlException("User " + user.getUsername() + " tried to set the value for a JSON path that's not configured as parameterizable in the training configuration " + trainConfig.getId() + ": " + parameters.keySet().stream().filter(parameter -> !parameterPaths.contains(parameter)).collect(Collectors.joining(", ")));
        }
        try {
            return trainConfig.getConfiguration().render(parameters);
        } catch (PathNotFoundException e) {
            throw new XnatMlException(user, e.getCause(), "Requested a JSON path that doesn't exist in the training configuration " + trainConfig.getId() + ": " + e.getMessage());
        } catch (IOException e) {
            throw new XnatMlException(user, e.getCause(), "An error occurred trying to convert the JSON text to a node object:\n\n" + e.getMessage());
        }
    }

    /**
     * {@inheritDoc}
     */
    @Nonnull
    @Override
    public JsonNode renderTrainingConfiguration(final UserI user, final MlTrainconfig trainConfig, final SetsCollection collection, final Map<String, String> parameters) {
        final JsonNode   configuration = renderTrainingConfiguration(user, trainConfig, parameters);
        final JsonNode   dataset       = renderDataset(trainConfig.getDataset(), collection, parameters);
        final ObjectNode rendered      = new ObjectNode(getSerializer().getObjectMapper().getNodeFactory());
        rendered.set("configuration", configuration);
        rendered.set("dataset", dataset);
        return rendered;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void deleteTrainingConfiguration(final UserI user, final String id) throws NotFoundException {
        deleteXnatMlObject(user, id);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void deleteTrainingConfiguration(final UserI user, final String project, final String idOrLabel) throws NotFoundException {
        deleteXnatMlObject(user, project, idOrLabel);
    }

    /**
     * {@inheritDoc}
     */
    @Nonnull
    private JsonNode renderDataset(final MlParameterizedconfig dataset, final SetsCollection collection, final Map<String, String> parameters) {
        // Allow overriding values in the stored partition with parameters for this training session.
        final Map<String, Integer> partitions = deserialize(dataset.getParameterizable());
        final Set<String>          common     = Sets.intersect(partitions.keySet(), parameters.keySet());

        final Map<String, Integer> parameterized;
        if (common.isEmpty()) {
            parameterized = partitions;
        } else {
            parameterized = Stream.concat(partitions.entrySet().stream(),
                                          parameters.entrySet().stream()
                                                    .filter(entry -> common.contains(entry.getKey()) && NumberUtils.isDigits(entry.getValue()))
                                                    .map(entry -> new AbstractMap.SimpleEntry<>(entry.getKey(), Integer.parseInt(entry.getValue()))))
                                  .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
        }
        if (log.isInfoEnabled()) {
            final String rendered = parameterized.keySet().stream().map(key -> key + ": " + parameterized.get(key)).collect(Collectors.joining(", "));
            if (common.isEmpty()) {
                log.info("Found partition map with {} entries: {}", parameterized.size(), rendered);
            } else {
                log.info("Found partition map with {} entries and {} parameter overrides ({}), with the resulting partition entries: {}", partitions.size(), common.size(), StringUtils.join(common, ", "), rendered);
            }
        }
        return getCollections().renderDataset(collection, partitions, (ObjectNode) dataset.getJsonTemplate());
    }

    private Map<String, Integer> deserialize(final String serialized) {
        try {
            return getSerializer().deserializeJson(serialized, PARAMETERIZABLE_MAP_REFERENCE);
        } catch (IOException e) {
            throw new XnatMlException(e, "An error occurred deserializing a value to the type Map<String, Integer>. The value was: " + serialized);
        }
    }

    private static final TypeReference<HashMap<String, Integer>> PARAMETERIZABLE_MAP_REFERENCE = new TypeReference<HashMap<String, Integer>>() {};

    private final DatasetCollectionService _collections;
    private final SerializerService        _serializer;
}
