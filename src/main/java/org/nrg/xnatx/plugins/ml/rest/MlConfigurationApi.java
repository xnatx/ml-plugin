/*
 * ml-plugin: org.nrg.xnatx.plugins.ml.rest.MlConfigurationApi
 * XNAT http://www.xnat.org
 * Copyright (c) 2005-2021, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.xnatx.plugins.ml.rest;

import static lombok.AccessLevel.PROTECTED;
import static org.springframework.http.MediaType.*;
import static org.springframework.web.bind.annotation.RequestMethod.*;

import com.fasterxml.jackson.databind.JsonNode;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.Getter;
import lombok.experimental.Accessors;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.nrg.framework.annotations.XapiRestController;
import org.nrg.xapi.exceptions.DataFormatException;
import org.nrg.xapi.exceptions.InsufficientPrivilegesException;
import org.nrg.xapi.exceptions.NotFoundException;
import org.nrg.xapi.exceptions.ResourceAlreadyExistsException;
import org.nrg.xapi.rest.AbstractExperimentXapiRestController;
import org.nrg.xapi.rest.XapiRequestMapping;
import org.nrg.xdat.model.MlParameterizedconfigI;
import org.nrg.xdat.model.MlTrainconfigI;
import org.nrg.xdat.om.MlParameterizedconfig;
import org.nrg.xdat.om.MlTrainconfig;
import org.nrg.xdat.om.SetsCollection;
import org.nrg.xdat.security.services.RoleHolder;
import org.nrg.xdat.security.services.UserManagementServiceI;
import org.nrg.xnatx.plugins.ml.services.MlConfigurationService;
import org.nrg.xnatx.plugins.datasets.services.DatasetCollectionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@Api("Machine Learning Training Configuration API")
@XapiRestController
@RequestMapping("/ml/config")
@Getter(PROTECTED)
@Accessors(prefix = "_")
@Slf4j
public class MlConfigurationApi extends AbstractExperimentXapiRestController<MlTrainconfig> {
    @Autowired
    protected MlConfigurationApi(final UserManagementServiceI userManagementService, final RoleHolder roleHolder, final NamedParameterJdbcTemplate template, final MlConfigurationService service, final DatasetCollectionService collections) throws IllegalAccessException, NoSuchFieldException {
        super(template, userManagementService, roleHolder);
        _service = service;
        _collections = collections;
    }

    @ApiOperation(value = "Get list of all training configurations accessible to the current user.", response = String.class, responseContainer = "Map")
    @ApiResponses({@ApiResponse(code = 200, message = "List of training configurations successfully retrieved."),
                   @ApiResponse(code = 401, message = "Must be authenticated to access the XNAT REST API."),
                   @ApiResponse(code = 403, message = "Not authorized to access this function."),
                   @ApiResponse(code = 500, message = "Unexpected error")})
    @XapiRequestMapping(produces = APPLICATION_JSON_VALUE, method = GET)
    @ResponseBody
    public Map<String, Map<String, String>> getAllTrainingConfigurations() {
        return getService().retrieveTrainingConfigurations(getSessionUser());
    }

    @ApiOperation(value = "Get list of training configurations for the specified project.", response = String.class, responseContainer = "Map")
    @ApiResponses({@ApiResponse(code = 200, message = "List of training configurations successfully retrieved."),
                   @ApiResponse(code = 401, message = "Must be authenticated to access the XNAT REST API."),
                   @ApiResponse(code = 403, message = "Not authorized to access this function."),
                   @ApiResponse(code = 404, message = "Couldn't find specified project."),
                   @ApiResponse(code = 500, message = "Unexpected error")})
    @XapiRequestMapping(value = "project/{projectId}", produces = APPLICATION_JSON_VALUE, method = GET)
    @ResponseBody
    public Map<String, String> getTrainingConfigurations(final @PathVariable String projectId) throws NotFoundException, InsufficientPrivilegesException {
        return getService().retrieveTrainingConfigurations(getSessionUser(), projectId);
    }

    @ApiOperation(value = "Create new training configuration object and return the ID of the newly created object.", response = MlTrainconfigI.class)
    @ApiResponses({@ApiResponse(code = 200, message = "Training configuration successfully created."),
                   @ApiResponse(code = 401, message = "Must be authenticated to access the XNAT REST API."),
                   @ApiResponse(code = 403, message = "Not authorized to access this function."),
                   @ApiResponse(code = 404, message = "Couldn't find specified project."),
                   @ApiResponse(code = 409, message = "A training configuration with the same label already exists in the specified project."),
                   @ApiResponse(code = 500, message = "Unexpected error")})
    @XapiRequestMapping(consumes = {APPLICATION_JSON_VALUE, APPLICATION_XML_VALUE}, produces = {APPLICATION_JSON_VALUE, APPLICATION_XML_VALUE}, method = POST)
    @ResponseBody
    public MlTrainconfigI createTrainingConfiguration(final @RequestBody MlTrainconfigI configuration) throws ResourceAlreadyExistsException, DataFormatException {
        if (StringUtils.isAnyBlank(configuration.getProject(), configuration.getLabel())) {
            throw new DataFormatException("You must provide a valid value for both project and label to create a new training configuration.");
        }
        return getService().createTrainingConfiguration(getSessionUser(), (MlTrainconfig) configuration);
    }

    @ApiOperation(value = "Create new training configuration object and return the ID of the newly created object.", response = MlTrainconfigI.class)
    @ApiResponses({@ApiResponse(code = 200, message = "Training configuration successfully created."),
                   @ApiResponse(code = 401, message = "Must be authenticated to access the XNAT REST API."),
                   @ApiResponse(code = 403, message = "Not authorized to access this function."),
                   @ApiResponse(code = 404, message = "Couldn't find specified project."),
                   @ApiResponse(code = 409, message = "A training configuration with the same label already exists in the specified project."),
                   @ApiResponse(code = 500, message = "Unexpected error")})
    @XapiRequestMapping(value = "project/{projectId}/{label}", consumes = {APPLICATION_JSON_VALUE, APPLICATION_XML_VALUE}, produces = {APPLICATION_JSON_VALUE, APPLICATION_XML_VALUE}, method = POST)
    @ResponseBody
    public MlTrainconfigI createTrainingConfiguration(final @PathVariable String projectId, final @PathVariable String label, final @RequestBody MlTrainconfigI configuration) throws ResourceAlreadyExistsException, DataFormatException {
        configuration.setProject(projectId);
        configuration.setLabel(label);
        return getService().createTrainingConfiguration(getSessionUser(), (MlTrainconfig) configuration);
    }

    @ApiOperation(value = "Create new training configuration object and return the ID of the newly created object.", response = MlTrainconfigI.class)
    @ApiResponses({@ApiResponse(code = 200, message = "Training configuration successfully created."),
                   @ApiResponse(code = 401, message = "Must be authenticated to access the XNAT REST API."),
                   @ApiResponse(code = 403, message = "Not authorized to access this function."),
                   @ApiResponse(code = 404, message = "Couldn't find specified project."),
                   @ApiResponse(code = 409, message = "A training configuration with the same label already exists in the specified project."),
                   @ApiResponse(code = 500, message = "Unexpected error")})
    @XapiRequestMapping(value = "project/{projectId}/{label}", consumes = MULTIPART_FORM_DATA_VALUE, produces = {APPLICATION_JSON_VALUE, APPLICATION_XML_VALUE}, method = POST)
    @ResponseBody
    public MlTrainconfigI createTrainingConfiguration(final @PathVariable String projectId, final @PathVariable String label, final @RequestParam JsonNode configuration, final @RequestParam(required = false) List<String> parameterizable, final @RequestParam(required = false) JsonNode dataset) throws ResourceAlreadyExistsException, NotFoundException, DataFormatException {
        final MlTrainconfig config = getMlTrainconfigWithProperties(null, projectId, label, configuration, parameterizable, dataset);
        return getService().createTrainingConfiguration(getSessionUser(), config);
    }

    @ApiOperation(value = "Get project training configuration object.", notes = "This returns the full XML-based MlTrainconfigI object, which includes the JSON training configuration template, as well as the list of parameterizable values.", response = MlTrainconfigI.class)
    @ApiResponses({@ApiResponse(code = 200, message = "Training configuration object successfully retrieved."),
                   @ApiResponse(code = 401, message = "Must be authenticated to access the XNAT REST API."),
                   @ApiResponse(code = 403, message = "Not authorized to access this function."),
                   @ApiResponse(code = 404, message = "Couldn't find referenced training configuration object."),
                   @ApiResponse(code = 500, message = "Unexpected error")})
    @XapiRequestMapping(value = "{id}", produces = {APPLICATION_JSON_VALUE, APPLICATION_XML_VALUE}, method = GET)
    @ResponseBody
    public MlTrainconfigI retrieveTrainingConfiguration(final @PathVariable String id) throws NotFoundException {
        return getService().retrieveTrainingConfiguration(getSessionUser(), id);
    }

    @ApiOperation(value = "Get project training configuration object.", notes = "This returns the full XML-based MlTrainconfigI object, which includes the JSON training configuration template, as well as the list of parameterizable values.", response = MlTrainconfigI.class)
    @ApiResponses({@ApiResponse(code = 200, message = "Training configuration object successfully retrieved."),
                   @ApiResponse(code = 401, message = "Must be authenticated to access the XNAT REST API."),
                   @ApiResponse(code = 403, message = "Not authorized to access this function."),
                   @ApiResponse(code = 404, message = "Couldn't find referenced training configuration object."),
                   @ApiResponse(code = 500, message = "Unexpected error")})
    @XapiRequestMapping(value = "project/{projectId}/{idOrLabel}", produces = {APPLICATION_JSON_VALUE, APPLICATION_XML_VALUE}, method = GET)
    @ResponseBody
    public MlTrainconfigI retrieveTrainingConfiguration(final @PathVariable String projectId, final @PathVariable String idOrLabel) throws NotFoundException {
        return getService().retrieveTrainingConfiguration(getSessionUser(), projectId, idOrLabel);
    }

    @ApiOperation(value = "Get project training configuration template.", notes = "This returns the JSON training configuration template.", response = JsonNode.class)
    @ApiResponses({@ApiResponse(code = 200, message = "Training configuration successfully retrieved."),
                   @ApiResponse(code = 401, message = "Must be authenticated to access the XNAT REST API."),
                   @ApiResponse(code = 403, message = "Not authorized to access this function."),
                   @ApiResponse(code = 404, message = "Couldn't find referenced training configuration."),
                   @ApiResponse(code = 500, message = "Unexpected error")})
    @XapiRequestMapping(value = "{id}/template", produces = APPLICATION_JSON_VALUE, method = GET)
    @ResponseBody
    public JsonNode retrieveTrainingConfigurationTemplate(final @PathVariable String id) throws NotFoundException {
        return getService().retrieveTrainingConfiguration(getSessionUser(), id).getConfiguration().getJsonTemplate();
    }

    @ApiOperation(value = "Get project training configuration template.", notes = "This returns the JSON training configuration template.", response = JsonNode.class)
    @ApiResponses({@ApiResponse(code = 200, message = "Training configuration successfully retrieved."),
                   @ApiResponse(code = 401, message = "Must be authenticated to access the XNAT REST API."),
                   @ApiResponse(code = 403, message = "Not authorized to access this function."),
                   @ApiResponse(code = 404, message = "Couldn't find referenced training configuration."),
                   @ApiResponse(code = 500, message = "Unexpected error")})
    @XapiRequestMapping(value = "project/{projectId}/{idOrLabel}/template", produces = APPLICATION_JSON_VALUE, method = GET)
    @ResponseBody
    public JsonNode retrieveTrainingConfigurationTemplate(final @PathVariable String projectId, final @PathVariable String idOrLabel) throws NotFoundException {
        return getService().retrieveTrainingConfiguration(getSessionUser(), projectId, idOrLabel).getConfiguration().getJsonTemplate();
    }

    @ApiOperation(value = "Get project training configuration parameters.", notes = "This returns the list of parameters for which the user can supply values when rendering the JSON training configuration template.", response = String.class, responseContainer = "Map")
    @ApiResponses({@ApiResponse(code = 200, message = "Training configuration parameters successfully retrieved."),
                   @ApiResponse(code = 401, message = "Must be authenticated to access the XNAT REST API."),
                   @ApiResponse(code = 403, message = "Not authorized to access this function."),
                   @ApiResponse(code = 404, message = "Couldn't find referenced training configuration."),
                   @ApiResponse(code = 500, message = "Unexpected error")})
    @XapiRequestMapping(value = "{id}/parameters", produces = APPLICATION_JSON_VALUE, method = GET)
    @ResponseBody
    public Map<String, Map<String, String>> retrieveTrainingConfigurationParameters(final @PathVariable String id) throws NotFoundException {
        return getService().retrieveTrainingConfiguration(getSessionUser(), id).getConfiguration().getParameterValues();
    }

    @ApiOperation(value = "Get project training configuration parameters.", notes = "This returns the list of parameters for which the user can supply values when rendering the JSON training configuration template.", response = String.class, responseContainer = "Map")
    @ApiResponses({@ApiResponse(code = 200, message = "Training configuration parameters successfully retrieved."),
                   @ApiResponse(code = 401, message = "Must be authenticated to access the XNAT REST API."),
                   @ApiResponse(code = 403, message = "Not authorized to access this function."),
                   @ApiResponse(code = 404, message = "Couldn't find referenced training configuration."),
                   @ApiResponse(code = 500, message = "Unexpected error")})
    @XapiRequestMapping(value = "project/{projectId}/{idOrLabel}/parameters", produces = APPLICATION_JSON_VALUE, method = GET)
    @ResponseBody
    public Map<String, Map<String, String>> retrieveTrainingConfigurationParameters(final @PathVariable String projectId, final @PathVariable String idOrLabel) throws NotFoundException {
        return getService().retrieveTrainingConfiguration(getSessionUser(), projectId, idOrLabel).getConfiguration().getParameterValues();
    }

    @ApiOperation(value = "Get training configuration dataset template.", notes = "This returns the JSON training configuration dataset template.", response = JsonNode.class)
    @ApiResponses({@ApiResponse(code = 200, message = "Training configuration dataset template successfully retrieved."),
                   @ApiResponse(code = 401, message = "Must be authenticated to access the XNAT REST API."),
                   @ApiResponse(code = 403, message = "Not authorized to access this function."),
                   @ApiResponse(code = 404, message = "Couldn't find referenced training configuration."),
                   @ApiResponse(code = 500, message = "Unexpected error")})
    @XapiRequestMapping(value = "{id}/dataset/template", produces = APPLICATION_JSON_VALUE, method = GET)
    @ResponseBody
    public JsonNode retrieveTrainingConfigurationDatasetTemplate(final @PathVariable String id) throws NotFoundException {
        return getService().retrieveTrainingConfiguration(getSessionUser(), id).getDataset().getJsonTemplate();
    }

    @ApiOperation(value = "Get training configuration dataset template.", notes = "This returns the JSON training configuration dataset template.", response = JsonNode.class)
    @ApiResponses({@ApiResponse(code = 200, message = "Training configuration dataset template successfully retrieved."),
                   @ApiResponse(code = 401, message = "Must be authenticated to access the XNAT REST API."),
                   @ApiResponse(code = 403, message = "Not authorized to access this function."),
                   @ApiResponse(code = 404, message = "Couldn't find referenced training configuration."),
                   @ApiResponse(code = 500, message = "Unexpected error")})
    @XapiRequestMapping(value = "project/{projectId}/{idOrLabel}/dataset/template", produces = APPLICATION_JSON_VALUE, method = GET)
    @ResponseBody
    public JsonNode retrieveTrainingConfigurationDatasetTemplate(final @PathVariable String projectId, final @PathVariable String idOrLabel) throws NotFoundException {
        return getService().retrieveTrainingConfiguration(getSessionUser(), projectId, idOrLabel).getDataset().getJsonTemplate();
    }

    @ApiOperation(value = "Get training configuration dataset parameters.", notes = "This returns the list of parameters for which the user can supply values when rendering the JSON training configuration dataset template.", response = String.class, responseContainer = "Map")
    @ApiResponses({@ApiResponse(code = 200, message = "Training configuration dataset parameters successfully retrieved."),
                   @ApiResponse(code = 401, message = "Must be authenticated to access the XNAT REST API."),
                   @ApiResponse(code = 403, message = "Not authorized to access this function."),
                   @ApiResponse(code = 404, message = "Couldn't find referenced training configuration."),
                   @ApiResponse(code = 500, message = "Unexpected error")})
    @XapiRequestMapping(value = "{id}/dataset/parameters", produces = APPLICATION_JSON_VALUE, method = GET)
    @ResponseBody
    public Map<String, String> retrieveTrainingConfigurationDatasetParameters(final @PathVariable String id) throws NotFoundException {
        return getService().retrieveTrainingConfiguration(getSessionUser(), id).getDataset().getParameterMap();
    }

    @ApiOperation(value = "Get training configuration dataset parameters.", notes = "This returns the list of parameters for which the user can supply values when rendering the JSON training configuration dataset template.", response = String.class, responseContainer = "Map")
    @ApiResponses({@ApiResponse(code = 200, message = "Training configuration dataset parameters successfully retrieved."),
                   @ApiResponse(code = 401, message = "Must be authenticated to access the XNAT REST API."),
                   @ApiResponse(code = 403, message = "Not authorized to access this function."),
                   @ApiResponse(code = 404, message = "Couldn't find referenced training configuration."),
                   @ApiResponse(code = 500, message = "Unexpected error")})
    @XapiRequestMapping(value = "project/{projectId}/{idOrLabel}/dataset/parameters", produces = APPLICATION_JSON_VALUE, method = GET)
    @ResponseBody
    public Map<String, String> retrieveTrainingConfigurationDatasetParameters(final @PathVariable String projectId, final @PathVariable String idOrLabel) throws NotFoundException {
        return getService().retrieveTrainingConfiguration(getSessionUser(), projectId, idOrLabel).getDataset().getParameterMap();
    }

    @ApiOperation(value = "Update training configuration.", response = MlTrainconfigI.class)
    @ApiResponses({@ApiResponse(code = 200, message = "Training configuration successfully updated."),
                   @ApiResponse(code = 401, message = "Must be authenticated to access the XNAT REST API."),
                   @ApiResponse(code = 403, message = "Not authorized to access this function."),
                   @ApiResponse(code = 404, message = "Couldn't find referenced training configuration."),
                   @ApiResponse(code = 500, message = "Unexpected error")})
    @XapiRequestMapping(value = "{id}", consumes = {APPLICATION_JSON_VALUE, APPLICATION_XML_VALUE}, produces = {APPLICATION_JSON_VALUE, APPLICATION_XML_VALUE}, method = PUT)
    @ResponseBody
    public MlTrainconfigI updateTrainingConfiguration(final @PathVariable String id, final @RequestBody MlTrainconfigI configuration) throws NotFoundException, DataFormatException {
        MlTrainconfig mlTrainconfig = (MlTrainconfig) configuration;
        validateEntityId(id, mlTrainconfig);
        return getService().updateTrainingConfiguration(getSessionUser(), mlTrainconfig);
    }

    @ApiOperation(value = "Update project training configuration.", response = MlTrainconfigI.class)
    @ApiResponses({@ApiResponse(code = 200, message = "Training configuration successfully updated."),
                   @ApiResponse(code = 401, message = "Must be authenticated to access the XNAT REST API."),
                   @ApiResponse(code = 403, message = "Not authorized to access this function."),
                   @ApiResponse(code = 404, message = "Couldn't find referenced training configuration."),
                   @ApiResponse(code = 500, message = "Unexpected error")})
    @XapiRequestMapping(value = "project/{projectId}/{idOrLabel}", consumes = {APPLICATION_JSON_VALUE, APPLICATION_XML_VALUE}, produces = {APPLICATION_JSON_VALUE, APPLICATION_XML_VALUE}, method = PUT)
    @ResponseBody
    public MlTrainconfigI updateTrainingConfiguration(final @PathVariable String projectId, final @PathVariable String idOrLabel, final @RequestBody MlTrainconfigI configuration) throws NotFoundException, DataFormatException {
        MlTrainconfig mlTrainconfig = (MlTrainconfig) configuration;
        validateEntityId(projectId, idOrLabel, mlTrainconfig);
        return getService().updateTrainingConfiguration(getSessionUser(), mlTrainconfig);
    }

    @ApiOperation(value = "Update project training configuration.", response = MlTrainconfigI.class)
    @ApiResponses({@ApiResponse(code = 200, message = "Training configuration successfully updated."),
                   @ApiResponse(code = 401, message = "Must be authenticated to access the XNAT REST API."),
                   @ApiResponse(code = 403, message = "Not authorized to access this function."),
                   @ApiResponse(code = 404, message = "Couldn't find referenced training configuration."),
                   @ApiResponse(code = 500, message = "Unexpected error")})
    @XapiRequestMapping(value = "{id}", consumes = MULTIPART_FORM_DATA_VALUE, produces = {APPLICATION_JSON_VALUE, APPLICATION_XML_VALUE}, method = PUT)
    @ResponseBody
    public MlTrainconfigI updateTrainingConfiguration(final @PathVariable String id, final @RequestParam(required = false) String label, final @RequestParam JsonNode configuration, final @RequestParam(required = false) List<String> parameterized, final @RequestParam(required = false) JsonNode dataset) throws NotFoundException, DataFormatException {
        validateId(id);
        final MlTrainconfig trainConfig = getMlTrainconfigWithProperties(id, null, label, configuration, parameterized, dataset);
        return getService().updateTrainingConfiguration(getSessionUser(), trainConfig);
    }

    @ApiOperation(value = "Update project training configuration.", response = MlTrainconfigI.class)
    @ApiResponses({@ApiResponse(code = 200, message = "Training configuration successfully updated."),
                   @ApiResponse(code = 401, message = "Must be authenticated to access the XNAT REST API."),
                   @ApiResponse(code = 403, message = "Not authorized to access this function."),
                   @ApiResponse(code = 404, message = "Couldn't find referenced training configuration."),
                   @ApiResponse(code = 500, message = "Unexpected error")})
    @XapiRequestMapping(value = "projects/{projectId}/{idOrLabel}", consumes = MULTIPART_FORM_DATA_VALUE, produces = {APPLICATION_JSON_VALUE, APPLICATION_XML_VALUE}, method = PUT)
    @ResponseBody
    public MlTrainconfigI updateTrainingConfiguration(final @PathVariable String projectId, final @PathVariable String idOrLabel, final @RequestParam(required = false) String label, final @RequestParam JsonNode configuration, final @RequestParam(required = false) List<String> parameterized, final @RequestParam(required = false) JsonNode dataset) throws NotFoundException, DataFormatException {
        validateId(projectId, idOrLabel);
        final MlTrainconfig trainConfig = getMlTrainconfigWithProperties(idOrLabel, projectId, label, configuration, parameterized, dataset);
        return getService().updateTrainingConfiguration(getSessionUser(), trainConfig);
    }

    @ApiOperation(value = "Update project training configuration.", response = MlTrainconfigI.class)
    @ApiResponses({@ApiResponse(code = 200, message = "Training configuration successfully updated."),
                   @ApiResponse(code = 401, message = "Must be authenticated to access the XNAT REST API."),
                   @ApiResponse(code = 403, message = "Not authorized to access this function."),
                   @ApiResponse(code = 404, message = "Couldn't find referenced training configuration."),
                   @ApiResponse(code = 500, message = "Unexpected error")})
    @XapiRequestMapping(value = "project/{projectId}/{idOrLabel}", consumes = MULTIPART_FORM_DATA_VALUE, produces = {APPLICATION_JSON_VALUE, APPLICATION_XML_VALUE}, method = PUT)
    @ResponseBody
    public MlTrainconfigI updateTrainingConfiguration(final @PathVariable String projectId, final @PathVariable String idOrLabel, final @RequestParam JsonNode configuration, final @RequestParam(required = false) List<String> parameterized) throws NotFoundException, DataFormatException {
        validateId(projectId, idOrLabel);
        return getService().updateTrainingConfiguration(getSessionUser(), projectId, idOrLabel, configuration, parameterized);
    }

    @ApiOperation(value = "Update project training configuration parameterizable properties.", response = MlTrainconfigI.class)
    @ApiResponses({@ApiResponse(code = 200, message = "Training configuration parameters successfully updated."),
                   @ApiResponse(code = 401, message = "Must be authenticated to access the XNAT REST API."),
                   @ApiResponse(code = 403, message = "Not authorized to access this function."),
                   @ApiResponse(code = 404, message = "Couldn't find referenced training configuration."),
                   @ApiResponse(code = 500, message = "Unexpected error")})
    @XapiRequestMapping(value = "{id}/parameters", consumes = APPLICATION_JSON_VALUE, produces = {APPLICATION_JSON_VALUE, APPLICATION_XML_VALUE}, method = PUT)
    @ResponseBody
    public MlTrainconfigI updateTrainingConfigurationParameters(final @PathVariable String id, final @RequestBody List<String> parameterized) throws NotFoundException, DataFormatException {
        validateId(id);
        return getService().updateTrainingConfigurationParameters(getSessionUser(), id, parameterized);
    }

    @ApiOperation(value = "Update project training configuration parameterizable properties.", response = MlTrainconfigI.class)
    @ApiResponses({@ApiResponse(code = 200, message = "Training configuration parameters successfully updated."),
                   @ApiResponse(code = 401, message = "Must be authenticated to access the XNAT REST API."),
                   @ApiResponse(code = 403, message = "Not authorized to access this function."),
                   @ApiResponse(code = 404, message = "Couldn't find referenced training configuration."),
                   @ApiResponse(code = 500, message = "Unexpected error")})
    @XapiRequestMapping(value = "project/{projectId}/{idOrLabel}/parameters", consumes = APPLICATION_JSON_VALUE, produces = {APPLICATION_JSON_VALUE, APPLICATION_XML_VALUE}, method = PUT)
    @ResponseBody
    public MlTrainconfigI updateTrainingConfigurationParameters(final @PathVariable String projectId, final @PathVariable String idOrLabel, final @RequestBody List<String> parameterized) throws NotFoundException, DataFormatException {
        validateId(projectId, idOrLabel);
        return getService().updateTrainingConfigurationParameters(getSessionUser(), projectId, idOrLabel, parameterized);
    }

    @ApiOperation(value = "Update project training configuration dataset template.", notes = "This returns the updated JSON training configuration.", response = JsonNode.class)
    @ApiResponses({@ApiResponse(code = 200, message = "Training configuration dataset template successfully updated."),
                   @ApiResponse(code = 401, message = "Must be authenticated to access the XNAT REST API."),
                   @ApiResponse(code = 403, message = "Not authorized to access this function."),
                   @ApiResponse(code = 404, message = "Couldn't find referenced training configuration."),
                   @ApiResponse(code = 500, message = "Unexpected error")})
    @XapiRequestMapping(value = "{id}/dataset/template", produces = APPLICATION_JSON_VALUE, method = PUT)
    @ResponseBody
    public MlTrainconfigI updateTrainingConfigurationDatasetTemplate(final @PathVariable String id, final @RequestBody JsonNode template) throws NotFoundException, DataFormatException {
        validateId(id);
        return updateDatasetTemplate(getService().retrieveTrainingConfiguration(getSessionUser(), id), template);
    }

    @ApiOperation(value = "Update project training configuration dataset template.", notes = "This returns the updated JSON training configuration.", response = JsonNode.class)
    @ApiResponses({@ApiResponse(code = 200, message = "Training configuration dataset template successfully updated."),
                   @ApiResponse(code = 401, message = "Must be authenticated to access the XNAT REST API."),
                   @ApiResponse(code = 403, message = "Not authorized to access this function."),
                   @ApiResponse(code = 404, message = "Couldn't find referenced training configuration."),
                   @ApiResponse(code = 500, message = "Unexpected error")})
    @XapiRequestMapping(value = "project/{projectId}/{idOrLabel}/dataset/template", produces = APPLICATION_JSON_VALUE, method = PUT)
    @ResponseBody
    public MlTrainconfigI updateTrainingConfigurationDatasetTemplate(final @PathVariable String projectId, final @PathVariable String idOrLabel, final @RequestBody JsonNode template) throws NotFoundException, DataFormatException {
        validateId(projectId, idOrLabel);
        return updateDatasetTemplate(getService().retrieveTrainingConfiguration(getSessionUser(), projectId, idOrLabel), template);
    }

    @ApiOperation(value = "Update project training configuration dataset template.", notes = "This returns the updated JSON training configuration.", response = JsonNode.class)
    @ApiResponses({@ApiResponse(code = 200, message = "Training configuration dataset template successfully updated."),
                   @ApiResponse(code = 401, message = "Must be authenticated to access the XNAT REST API."),
                   @ApiResponse(code = 403, message = "Not authorized to access this function."),
                   @ApiResponse(code = 404, message = "Couldn't find referenced training configuration."),
                   @ApiResponse(code = 500, message = "Unexpected error")})
    @XapiRequestMapping(value = "{id}/dataset/parameters", produces = APPLICATION_JSON_VALUE, method = PUT)
    @ResponseBody
    public MlTrainconfigI updateTrainingConfigurationDatasetParameters(final @PathVariable String id, final @RequestBody JsonNode parameterizable) throws NotFoundException, DataFormatException {
        validateId(id);
        return updateDatasetParameters(getService().retrieveTrainingConfiguration(getSessionUser(), id), parameterizable);
    }

    @ApiOperation(value = "Update project training configuration dataset template.", notes = "This returns the updated JSON training configuration.", response = JsonNode.class)
    @ApiResponses({@ApiResponse(code = 200, message = "Training configuration dataset template successfully updated."),
                   @ApiResponse(code = 401, message = "Must be authenticated to access the XNAT REST API."),
                   @ApiResponse(code = 403, message = "Not authorized to access this function."),
                   @ApiResponse(code = 404, message = "Couldn't find referenced training configuration."),
                   @ApiResponse(code = 500, message = "Unexpected error")})
    @XapiRequestMapping(value = "project/{projectId}/{idOrLabel}/dataset/parameters", produces = APPLICATION_JSON_VALUE, method = PUT)
    @ResponseBody
    public MlTrainconfigI updateTrainingConfigurationDatasetParameters(final @PathVariable String projectId, final @PathVariable String idOrLabel, final @RequestBody JsonNode parameterizable) throws NotFoundException, DataFormatException {
        validateId(projectId, idOrLabel);
        return updateDatasetParameters(getService().retrieveTrainingConfiguration(getSessionUser(), projectId, idOrLabel), parameterizable);
    }

    @ApiOperation(value = "Delete project training configuration.")
    @ApiResponses({@ApiResponse(code = 200, message = "Training configuration successfully deleted."),
                   @ApiResponse(code = 401, message = "Must be authenticated to access the XNAT REST API."),
                   @ApiResponse(code = 403, message = "Not authorized to access this function."),
                   @ApiResponse(code = 404, message = "Couldn't find referenced training configuration."),
                   @ApiResponse(code = 500, message = "Unexpected error")})
    @XapiRequestMapping(value = "{id}", produces = APPLICATION_JSON_VALUE, method = DELETE)
    public void deleteTrainingConfiguration(final @PathVariable String id) throws NotFoundException {
        getService().deleteTrainingConfiguration(getSessionUser(), id);
    }

    @ApiOperation(value = "Delete project training configuration.")
    @ApiResponses({@ApiResponse(code = 200, message = "Training configuration successfully deleted."),
                   @ApiResponse(code = 401, message = "Must be authenticated to access the XNAT REST API."),
                   @ApiResponse(code = 403, message = "Not authorized to access this function."),
                   @ApiResponse(code = 404, message = "Couldn't find referenced training configuration."),
                   @ApiResponse(code = 500, message = "Unexpected error")})
    @XapiRequestMapping(value = "project/{projectId}/{idOrLabel}", produces = APPLICATION_JSON_VALUE, method = DELETE)
    public void deleteTrainingConfiguration(final @PathVariable String projectId, final @PathVariable String idOrLabel) throws NotFoundException {
        getService().deleteTrainingConfiguration(getSessionUser(), projectId, idOrLabel);
    }

    @ApiOperation(value = "Renders the project training configuration.", notes = "This returns the training configuration template, substituting the values in the parameter map for the value in the JSON training configuration template at the specified path.", response = JsonNode.class)
    @ApiResponses({@ApiResponse(code = 200, message = "Training configuration successfully rendered."),
                   @ApiResponse(code = 401, message = "Must be authenticated to access the XNAT REST API."),
                   @ApiResponse(code = 403, message = "Not authorized to access this function."),
                   @ApiResponse(code = 404, message = "Couldn't find referenced training configuration."),
                   @ApiResponse(code = 500, message = "Unexpected error")})
    @XapiRequestMapping(value = "{id}/render", consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE, method = PUT)
    @ResponseBody
    public JsonNode renderTrainingConfiguration(final @PathVariable String id, final @RequestBody(required = false) Map<String, String> parameters) throws NotFoundException {
        validateId(id);
        return getService().renderTrainingConfiguration(getSessionUser(), id, parameters);
    }

    @ApiOperation(value = "Renders the project training configuration.", notes = "This returns the training configuration template, substituting the values in the parameter map for the value in the JSON training configuration template at the specified path.", response = JsonNode.class)
    @ApiResponses({@ApiResponse(code = 200, message = "Training configuration successfully retrieved."),
                   @ApiResponse(code = 401, message = "Must be authenticated to access the XNAT REST API."),
                   @ApiResponse(code = 403, message = "Not authorized to access this function."),
                   @ApiResponse(code = 404, message = "Couldn't find referenced training configuration."),
                   @ApiResponse(code = 500, message = "Unexpected error")})
    @XapiRequestMapping(value = "project/{projectId}/{idOrLabel}/render", consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE, method = PUT)
    @ResponseBody
    public JsonNode renderTrainingConfiguration(final @PathVariable String projectId, final @PathVariable String idOrLabel, final @RequestBody(required = false) Map<String, String> parameters) throws NotFoundException {
        validateId(projectId, idOrLabel);
        return getService().renderTrainingConfiguration(getSessionUser(), projectId, idOrLabel, parameters);
    }

    @ApiOperation(value = "Renders the project training configuration.", notes = "This returns the training configuration template, substituting the values in the parameter map for the value in the JSON training configuration template at the specified path.", response = JsonNode.class)
    @ApiResponses({@ApiResponse(code = 200, message = "Training configuration successfully rendered."),
                   @ApiResponse(code = 401, message = "Must be authenticated to access the XNAT REST API."),
                   @ApiResponse(code = 403, message = "Not authorized to access this function."),
                   @ApiResponse(code = 404, message = "Couldn't find referenced training configuration."),
                   @ApiResponse(code = 500, message = "Unexpected error")})
    @XapiRequestMapping(value = "{id}/render/{collectionId}", consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE, method = PUT)
    @ResponseBody
    public JsonNode renderFullTrainingConfiguration(final @PathVariable String id, final @PathVariable String collectionId, final @RequestBody(required = false) Map<String, String> parameters) throws NotFoundException {
        validateId(id);
        return getService().renderTrainingConfiguration(getSessionUser(), (MlTrainconfig) retrieveTrainingConfiguration(id), getCollections().findById(getSessionUser(), collectionId), parameters);
    }

    @ApiOperation(value = "Renders the project training configuration.", notes = "This returns the training configuration template, substituting the values in the parameter map for the value in the JSON training configuration template at the specified path.", response = JsonNode.class)
    @ApiResponses({@ApiResponse(code = 200, message = "Training configuration successfully retrieved."),
                   @ApiResponse(code = 401, message = "Must be authenticated to access the XNAT REST API."),
                   @ApiResponse(code = 403, message = "Not authorized to access this function."),
                   @ApiResponse(code = 404, message = "Couldn't find referenced training configuration."),
                   @ApiResponse(code = 500, message = "Unexpected error")})
    @XapiRequestMapping(value = "project/{projectId}/{idOrLabel}/render/{collectionId}", consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE, method = PUT)
    @ResponseBody
    public JsonNode renderFullTrainingConfiguration(final @PathVariable String projectId, final @PathVariable String idOrLabel, final @PathVariable String collectionId, final @RequestBody(required = false) Map<String, String> parameters) throws NotFoundException {
        final MlTrainconfigI config     = MlTrainconfig.getMlTrainconfigsById(getEntityIdFromIdOrLabel(projectId, idOrLabel), getSessionUser(), false);
        final SetsCollection   collection = SetsCollection.getSetsCollectionsById(collectionId, getSessionUser(), false);
        return getService().renderTrainingConfiguration(getSessionUser(), (MlTrainconfig) config, collection, parameters);
    }

    private MlTrainconfigI updateDatasetTemplate(final MlTrainconfig config, final JsonNode template) throws NotFoundException, DataFormatException {
        config.getDataset().setJsonTemplate(template);
        getService().updateTrainingConfiguration(getSessionUser(), config);
        return config;
    }

    private MlTrainconfigI updateDatasetParameters(final MlTrainconfig config, final JsonNode parameterizable) throws NotFoundException, DataFormatException {
        config.getDataset().setJsonParameterizable(parameterizable);
        getService().updateTrainingConfiguration(getSessionUser(), config);
        return config;
    }

    @NotNull
    private MlTrainconfig getMlTrainconfigWithProperties(final String id, final String project, final String label, final JsonNode configuration, final List<String> parameterizable, final JsonNode dataset) throws NotFoundException, DataFormatException {
        final boolean          hasId      = StringUtils.isNotBlank(id);
        final boolean          hasProject = StringUtils.isNotBlank(project);
        final boolean          hasLabel   = StringUtils.isNotBlank(label);
        final MlTrainconfig config     = hasId ? new MlTrainconfig() : (hasProject ? (hasLabel ? _service.retrieveTrainingConfiguration(getSessionUser(), project, label) : _service.retrieveTrainingConfiguration(getSessionUser(), project, id)) : _service.retrieveTrainingConfiguration(getSessionUser(), id));
        if (hasProject) {
            if (StringUtils.isBlank(config.getProject())) {
                config.setProject(project);
            } else if (!StringUtils.equals(project, config.getProject())) {
                throw new DataFormatException("You can't change the project for an existing training configuration");
            }
        }
        if (hasLabel && !StringUtils.equals(label, config.getLabel())) {
            config.setLabel(label);
        }
        if (configuration != null) {
            config.setConfiguration(configuration.asText(), parameterizable);
        }
        if (dataset != null) {
            final MlParameterizedconfig datasetConfig = new MlParameterizedconfig();
            datasetConfig.setFromJson(dataset);
            try {
                config.setDataset((MlParameterizedconfigI) datasetConfig);
            } catch (Exception e) {
                log.error("An error occurred trying to set the dataset configuration.");
            }
        }
        return config;
    }

    private final MlConfigurationService   _service;
    private final DatasetCollectionService _collections;
}
