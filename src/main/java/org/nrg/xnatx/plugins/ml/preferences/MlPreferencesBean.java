/*
 * ml-plugin: org.nrg.xnatx.plugins.ml.preferences.MlPreferencesBean
 * XNAT http://www.xnat.org
 * Copyright (c) 2005-2021, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.xnatx.plugins.ml.preferences;

import lombok.extern.slf4j.Slf4j;
import org.nrg.framework.configuration.ConfigPaths;
import org.nrg.framework.utilities.OrderedProperties;
import org.nrg.prefs.annotations.NrgPreference;
import org.nrg.prefs.annotations.NrgPreferenceBean;
import org.nrg.prefs.beans.AbstractPreferenceBean;
import org.nrg.prefs.exceptions.InvalidPreferenceName;
import org.nrg.prefs.services.NrgPreferenceService;
import org.springframework.beans.factory.annotation.Autowired;

@NrgPreferenceBean(toolId = "xnat-ml", toolName = "XNAT Machine Learning Configuration")
@Slf4j
public class MlPreferencesBean extends AbstractPreferenceBean {
    @Autowired
    public MlPreferencesBean(final NrgPreferenceService preferenceService, final ConfigPaths configFolderPaths, final OrderedProperties orderedProperties) {
        super(preferenceService, configFolderPaths, orderedProperties);
    }

    @NrgPreference(defaultValue = "2000")
    public int getDefaultEpochCount() {
        return getIntegerValue("defaultEpochCount");
    }

    @SuppressWarnings("unused")
    public void setDefaultEpochCount(final int defaultEpochCount) {
        try {
            setIntegerValue(defaultEpochCount, "defaultEpochCount");
        } catch (InvalidPreferenceName invalidPreferenceName) {
            //
        }
    }

    @NrgPreference(defaultValue = "false")
    public Boolean getTensorBoardEnabled() {
        return getBooleanValue("tensorBoardEnabled");
    }

    @SuppressWarnings("unused")
    public void setTensorBoardEnabled(final Boolean enabled) {
        if (enabled != null) {
            try {
                setBooleanValue(enabled, "tensorBoardEnabled");
            } catch (InvalidPreferenceName invalidPreferenceName) {
                //
            }
        }
    }

}
