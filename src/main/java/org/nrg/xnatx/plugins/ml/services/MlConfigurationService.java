/*
 * ml-plugin: org.nrg.xnatx.plugins.ml.services.MlConfigurationService
 * XNAT http://www.xnat.org
 * Copyright (c) 2005-2021, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.xnatx.plugins.ml.services;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.Nonnull;
import org.nrg.xapi.exceptions.DataFormatException;
import org.nrg.xapi.exceptions.InsufficientPrivilegesException;
import org.nrg.xapi.exceptions.NotFoundException;
import org.nrg.xapi.exceptions.ResourceAlreadyExistsException;
import org.nrg.xdat.om.MlTrainconfig;
import org.nrg.xdat.om.SetsCollection;
import org.nrg.xft.security.UserI;

@SuppressWarnings("unused")
public interface MlConfigurationService {
    TypeReference<HashMap<String, Integer>>           PARAMETERIZABLE_MAP_REFERENCE = new TypeReference<HashMap<String, Integer>>() {};
    TypeReference<ArrayList<HashMap<String, String>>> COLLECTION_REFERENCE          = new TypeReference<ArrayList<HashMap<String, String>>>() {};

    /**
     * Creates the submitted training configuration.
     *
     * @param user          The user requesting the configuration.
     * @param configuration The configuration to be created.
     *
     * @return The newly created training configuration.
     */
    @Nonnull
    MlTrainconfig createTrainingConfiguration(final UserI user, final MlTrainconfig configuration) throws ResourceAlreadyExistsException, DataFormatException;

    /**
     * Creates the submitted training configuration from JSON.
     *
     * @param user          The user requesting the configuration.
     * @param project       The ID of the configuration's project.
     * @param label         The configuration label.
     * @param configuration The configuration to be created.
     * @param parameterized JSON paths indicating values that can be parameterized for training.
     *
     * @return The newly created training configuration.
     */
    @Nonnull
    MlTrainconfig createTrainingConfiguration(final UserI user, final String project, final String label, final String configuration, final List<String> parameterized) throws ResourceAlreadyExistsException, DataFormatException;

    /**
     * Creates the submitted training configuration from a JSON string.
     *
     * @param user          The user requesting the configuration.
     * @param project       The ID of the configuration's project.
     * @param label         The configuration label.
     * @param configuration The configuration to be created.
     * @param parameterized JSON paths indicating values that can be parameterized for training.
     *
     * @return The newly created training configuration.
     */
    @Nonnull
    MlTrainconfig createTrainingConfiguration(final UserI user, final String project, final String label, final String configuration, final String... parameterized) throws ResourceAlreadyExistsException, DataFormatException;

    /**
     * Creates the submitted training configuration from JSON.
     *
     * @param user          The user requesting the configuration.
     * @param project       The ID of the configuration's project.
     * @param label         The configuration label.
     * @param configuration The configuration to be created.
     * @param parameterized JSON paths indicating values that can be parameterized for training.
     *
     * @return The newly created training configuration.
     */
    @Nonnull
    MlTrainconfig createTrainingConfiguration(final UserI user, final String project, final String label, final JsonNode configuration, final List<String> parameterized) throws ResourceAlreadyExistsException, DataFormatException;

    /**
     * Creates the submitted training configuration from JSON.
     *
     * @param user          The user requesting the configuration.
     * @param project       The ID of the configuration's project.
     * @param label         The configuration label.
     * @param configuration The configuration to be created.
     * @param parameterized JSON paths indicating values that can be parameterized for training.
     *
     * @return The newly created training configuration.
     */
    @Nonnull
    MlTrainconfig createTrainingConfiguration(final UserI user, final String project, final String label, final JsonNode configuration, final String... parameterized) throws ResourceAlreadyExistsException, DataFormatException;

    /**
     * Gets the project, ID, and label of all training configurations accessible to the user.
     *
     * @param user The user requesting the configurations.
     *
     * @return The project, ID, and label of all training configurations accessible to the user.
     */
    @Nonnull
    Map<String, Map<String, String>> retrieveTrainingConfigurations(final UserI user);

    /**
     * Gets the IDs and labels of the training configurations associated with the indicated project.
     *
     * @param user    The user requesting the configurations.
     * @param project The ID of the project for which you want to retrieve the training configurations.
     *
     * @return The IDs and labels for the training configuration for the indicated project.
     */
    @Nonnull
    Map<String, String> retrieveTrainingConfigurations(final UserI user, final String project) throws NotFoundException, InsufficientPrivilegesException;

    /**
     * Gets the training configuration associated with the indicated project with the specified ID or
     * label within the project.
     *
     * @param user The user requesting the configuration.
     * @param id   The ID of the configuration object.
     *
     * @return The training configuration with the indicated ID, null if not found.
     */
    @Nonnull
    MlTrainconfig retrieveTrainingConfiguration(final UserI user, final String id) throws NotFoundException;

    /**
     * Gets the training configuration associated with the indicated project with the specified ID or
     * label within the project.
     *
     * @param user      The user requesting the configuration.
     * @param project   The ID of the project for which you want to retrieve the training configuration.
     * @param idOrLabel The ID or name within the project of the configuration object.
     *
     * @return The training configuration for the indicated project and type, null if not found.
     */
    @Nonnull
    MlTrainconfig retrieveTrainingConfiguration(final UserI user, final String project, final String idOrLabel) throws NotFoundException;

    /**
     * Updates the submitted training configuration in the indicated project.
     *
     * @param user          The user requesting the configuration.
     * @param configuration The configuration to be updated.
     *
     * @return The updated training configuration.
     */
    @Nonnull
    MlTrainconfig updateTrainingConfiguration(final UserI user, final MlTrainconfig configuration) throws NotFoundException, DataFormatException;

    /**
     * Updates the indicated training configuration.
     *
     * @param user          The user requesting the configuration.
     * @param id            The ID of the configuration object.
     * @param configuration The configuration to be updated.
     * @param parameterized JSON paths indicating values that can be parameterized for training.
     *
     * @return The updated training configuration.
     */
    @Nonnull
    MlTrainconfig updateTrainingConfiguration(final UserI user, final String id, final String configuration, final List<String> parameterized) throws NotFoundException, DataFormatException;

    /**
     * Updates the indicated training configuration.
     *
     * @param user          The user requesting the configuration.
     * @param id            The ID of the configuration object.
     * @param configuration The configuration to be updated.
     * @param parameterized JSON paths indicating values that can be parameterized for training.
     *
     * @return The updated training configuration.
     */
    @Nonnull
    MlTrainconfig updateTrainingConfiguration(final UserI user, final String id, final String configuration, final String... parameterized) throws NotFoundException, DataFormatException;

    /**
     * Updates the indicated training configuration.
     *
     * @param user          The user requesting the configuration.
     * @param id            The ID of the configuration object.
     * @param configuration The configuration to be updated.
     * @param parameterized JSON paths indicating values that can be parameterized for training.
     *
     * @return The updated training configuration.
     */
    @Nonnull
    MlTrainconfig updateTrainingConfiguration(final UserI user, final String id, final JsonNode configuration, final List<String> parameterized) throws NotFoundException, DataFormatException;

    /**
     * Updates the indicated training configuration.
     *
     * @param user          The user requesting the configuration.
     * @param id            The ID of the configuration object.
     * @param configuration The configuration to be updated.
     * @param parameterized JSON paths indicating values that can be parameterized for training.
     *
     * @return The updated training configuration.
     */
    @Nonnull
    MlTrainconfig updateTrainingConfiguration(final UserI user, final String id, final JsonNode configuration, final String... parameterized) throws NotFoundException, DataFormatException;

    /**
     * Updates the indicated training configuration.
     *
     * @param user          The user requesting the configuration.
     * @param project       The ID of the project for which you want to retrieve the training configuration.
     * @param idOrLabel     The ID or name within the project of the configuration object.
     * @param configuration The configuration to be updated.
     * @param parameterized JSON paths indicating values that can be parameterized for training.
     *
     * @return The updated training configuration.
     */
    @Nonnull
    MlTrainconfig updateTrainingConfiguration(final UserI user, final String project, final String idOrLabel, final String configuration, final List<String> parameterized) throws NotFoundException, DataFormatException;

    /**
     * Updates the indicated training configuration.
     *
     * @param user          The user requesting the configuration.
     * @param project       The ID of the project for which you want to retrieve the training configuration.
     * @param idOrLabel     The ID or name within the project of the configuration object.
     * @param configuration The configuration to be updated.
     * @param parameterized JSON paths indicating values that can be parameterized for training.
     *
     * @return The updated training configuration.
     */
    @Nonnull
    MlTrainconfig updateTrainingConfiguration(final UserI user, final String project, final String idOrLabel, final String configuration, final String... parameterized) throws NotFoundException, DataFormatException;

    /**
     * Updates the indicated training configuration.
     *
     * @param user          The user requesting the configuration.
     * @param project       The ID of the project for which you want to retrieve the training configuration.
     * @param idOrLabel     The ID or name within the project of the configuration object.
     * @param configuration The configuration to be updated.
     * @param parameterized JSON paths indicating values that can be parameterized for training.
     *
     * @return The updated training configuration.
     */
    @Nonnull
    MlTrainconfig updateTrainingConfiguration(final UserI user, final String project, final String idOrLabel, final JsonNode configuration, final List<String> parameterized) throws NotFoundException, DataFormatException;

    /**
     * Updates the indicated training configuration.
     *
     * @param user          The user requesting the configuration.
     * @param project       The ID of the project for which you want to retrieve the training configuration.
     * @param idOrLabel     The ID or name within the project of the configuration object.
     * @param configuration The configuration to be updated.
     * @param parameterized JSON paths indicating values that can be parameterized for training.
     *
     * @return The updated training configuration.
     */
    @Nonnull
    MlTrainconfig updateTrainingConfiguration(final UserI user, final String project, final String idOrLabel, final JsonNode configuration, final String... parameterized) throws NotFoundException, DataFormatException;

    /**
     * Updates the indicated training configuration parameterizable properties.
     *
     * @param user          The user requesting the configuration.
     * @param id            The ID of the configuration object.
     * @param parameterized JSON paths indicating values that can be parameterized for training.
     *
     * @return The updated training configuration.
     */
    @Nonnull
    MlTrainconfig updateTrainingConfigurationParameters(final UserI user, final String id, final List<String> parameterized) throws NotFoundException, DataFormatException;

    /**
     * Updates the indicated training configuration parameterizable properties.
     *
     * @param user          The user requesting the configuration.
     * @param project       The ID of the configuration's project.
     * @param idOrLabel     The ID or name within the project of the configuration object.
     * @param parameterized JSON paths indicating values that can be parameterized for training.
     *
     * @return The updated training configuration.
     */
    @Nonnull
    MlTrainconfig updateTrainingConfigurationParameters(final UserI user, final String project, final String idOrLabel, final List<String> parameterized) throws NotFoundException, DataFormatException;

    /**
     * Deletes the indicated training configuration.
     *
     * @param user The user requesting the configuration.
     * @param id   The ID of the configuration object.
     */
    void deleteTrainingConfiguration(final UserI user, final String id) throws NotFoundException;

    /**
     * Deletes the indicated training configuration in the indicated project.
     *
     * @param user      The user requesting the configuration.
     * @param project   The ID of the project for which you want to retrieve the training configuration.
     * @param idOrLabel The ID or name within the project of the configuration object.
     */
    void deleteTrainingConfiguration(final UserI user, final String project, final String idOrLabel) throws NotFoundException;

    /**
     * Renders the indicated training configuration, substituting values from the submitted map for the corresponding JSON paths.
     *
     * @param user       The user requesting the rendered configuration.
     * @param id         The ID of the configuration object.
     * @param parameters A map of JSON paths and values to be rendered into the training configuration template.
     *
     * @return The rendered training configuration.
     */
    @Nonnull
    JsonNode renderTrainingConfiguration(final UserI user, final String id, final Map<String, String> parameters) throws NotFoundException;

    /**
     * Renders the indicated training configuration, substituting values from the submitted map for the corresponding JSON paths. Note that this method does
     * <i>not</i> return the rendered dataset. If you want to render the full configuration with dataset, call {@link #renderTrainingConfiguration(UserI,
     * MlTrainconfig, SetsCollection, Map)} instead.
     *
     * @param user       The user requesting the rendered configuration.
     * @param project    The ID of the configuration's project.
     * @param idOrLabel  The ID or name within the project of the configuration object.
     * @param parameters A map of JSON paths and values to be rendered into the training configuration template.
     *
     * @return The rendered training configuration.
     */
    @Nonnull
    JsonNode renderTrainingConfiguration(final UserI user, final String project, final String idOrLabel, final Map<String, String> parameters) throws NotFoundException;

    /**
     * Renders the indicated training configuration, substituting values from the submitted map for the corresponding JSON paths. Note that this method does
     * <i>not</i> return the rendered dataset. If you want to render the full configuration with dataset, call {@link #renderTrainingConfiguration(UserI,
     * MlTrainconfig, SetsCollection, Map)} instead.
     *
     * @param user        The user requesting the rendered configuration.
     * @param trainConfig The training configuration.
     * @param parameters  A map of JSON paths and values to be rendered into the training configuration template.
     *
     * @return The rendered training configuration.
     */
    @Nonnull
    JsonNode renderTrainingConfiguration(final UserI user, final MlTrainconfig trainConfig, final Map<String, String> parameters);

    /**
     * Renders the indicated training configuration, including the partitioned dataset, substituting values from the submitted map for the corresponding JSON paths.
     *
     * @param user        The user requesting the rendered configuration.
     * @param trainConfig The training configuration.
     * @param collection  The dataset to use for rendering.
     * @param parameters  A map of JSON paths and values to be rendered into the training configuration template.
     *
     * @return The rendered training configuration.
     */
    @Nonnull
    JsonNode renderTrainingConfiguration(final UserI user, final MlTrainconfig trainConfig, final SetsCollection collection, final Map<String, String> parameters);
}
