/*
 * ml-plugin: org.nrg.xnatx.plugins.ml.models.DataSetExperiment
 * XNAT http://www.xnat.org
 * Copyright (c) 2005-2021, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.xnatx.plugins.ml.models;

import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.experimental.Accessors;

@ApiModel(description = "Provides a container for ML data experiment resources.")
@Data
@Accessors(prefix = "_")
public class DataSetExperiment {
    private final String _image;
    private final String _label;
}
