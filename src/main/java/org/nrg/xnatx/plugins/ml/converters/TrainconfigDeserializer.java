/*
 * ml-plugin: org.nrg.xnatx.plugins.ml.converters.TrainconfigDeserializer
 * XNAT http://www.xnat.org
 * Copyright (c) 2005-2021, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.xnatx.plugins.ml.converters;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.DeserializationContext;
import java.io.IOException;
import lombok.extern.slf4j.Slf4j;
import org.nrg.xdat.model.MlParameterizedconfigI;
import org.nrg.xdat.om.MlParameterizedconfig;
import org.nrg.xdat.om.MlTrainconfig;
import org.nrg.xnatx.plugins.datasets.converters.DatasetDeserializer;

@Slf4j
public class TrainconfigDeserializer extends DatasetDeserializer<MlTrainconfig> {
    public TrainconfigDeserializer() {
        super(MlTrainconfig.class);
    }

    @Override
    public MlTrainconfig deserialize(final JsonParser parser, final DeserializationContext context) throws IOException {
        if (parser.getCurrentToken() != JsonToken.START_OBJECT) {
            throw new IOException("invalid start marker");
        }

        final MlTrainconfig configuration = new MlTrainconfig();
        while (parser.nextToken() != JsonToken.END_OBJECT) {
            final String field = parser.getCurrentName();
            parser.nextToken();  //move to next token in string
            switch (field) {
                case "id":
                    configuration.setId(parser.getText());
                    break;
                case "project":
                    configuration.setProject(parser.getText());
                    break;
                case "label":
                    configuration.setLabel(parser.getText());
                    break;
                case "type":
                    configuration.setType(parser.getText());
                    break;
                case "configuration":
                    log.info("Trying to do the configuration now");
                    try {
                        configuration.setConfiguration((MlParameterizedconfigI) context.readValue(parser, MlParameterizedconfig.class));
                    } catch (Exception e) {
                        log.error("An error occurred trying to set the configuration", e);
                    }
                    break;
                case "dataset":
                    log.info("Trying to do the dataset now");
                    try {
                        configuration.setDataset((MlParameterizedconfigI) context.readValue(parser, MlParameterizedconfig.class));
                    } catch (Exception e) {
                        log.error("An error occurred trying to set the dataset", e);
                    }
                    break;
                case "note":
                    configuration.setNote(parser.getText());
                    break;
            }
        }
        return configuration;
    }
}
