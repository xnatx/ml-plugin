/*
 * ml-plugin: org.nrg.xnatx.plugins.ml.exceptions.XnatMlException
 * XNAT http://www.xnat.org
 * Copyright (c) 2005-2021, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.xnatx.plugins.ml.exceptions;

import java.util.HashMap;
import java.util.Map;
import lombok.Getter;
import lombok.experimental.Accessors;
import org.nrg.xft.security.UserI;

@SuppressWarnings("unused")
@Getter
@Accessors(prefix = "_")
public class XnatMlException extends RuntimeException {
    public XnatMlException() {
        this(null, null, null);
    }

    public XnatMlException(final String message) {
        this(null, null, message);
    }

    public XnatMlException(final Throwable cause) {
        this(null, cause, null);
    }

    public XnatMlException(final Throwable cause, final String message) {
        this(null, cause, message);
    }

    public XnatMlException(final UserI user) {
        this(user, null, null);
    }

    public XnatMlException(final UserI user, final String message) {
        this(user, null, message);
    }

    public XnatMlException(final UserI user, final Throwable cause) {
        this(user, cause, null);
    }

    public XnatMlException(final UserI user, final Throwable cause, final String message) {
        super(message, cause);
        _user = null;
    }

    public Object getParameter(final String name) {
        return _parameters.get(name);
    }

    public void setParameter(final String name, final Object value) {
        _parameters.put(name, value);
    }

    private final Map<String, Object> _parameters = new HashMap<>();

    private final UserI _user;
}
