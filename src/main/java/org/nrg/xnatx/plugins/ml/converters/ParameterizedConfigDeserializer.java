/*
 * ml-plugin: org.nrg.xnatx.plugins.ml.converters.ParameterizedConfigDeserializer
 * XNAT http://www.xnat.org
 * Copyright (c) 2005-2021, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.xnatx.plugins.ml.converters;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.DeserializationContext;
import java.io.IOException;
import org.nrg.xdat.om.MlParameterizedconfig;
import org.nrg.xnatx.plugins.datasets.converters.DatasetDeserializer;

public class ParameterizedConfigDeserializer extends DatasetDeserializer<MlParameterizedconfig> {
    public ParameterizedConfigDeserializer() {
        super(MlParameterizedconfig.class);
    }

    @Override
    public MlParameterizedconfig deserialize(final JsonParser parser, final DeserializationContext context) throws IOException {
        if (parser.getCurrentToken() != JsonToken.START_OBJECT) {
            throw new IOException("invalid start marker");
        }

        final MlParameterizedconfig config = new MlParameterizedconfig();
        while (parser.nextToken() != JsonToken.END_OBJECT) {
            final String field = parser.getCurrentName();
            parser.nextToken();  //move to next token in string
            switch (field) {
                case "template":
                    config.setTemplate(stringify(parser, context));
                    break;
                case "parameterizable":
                    config.setParameterizable(stringify(parser, context));
                    break;
            }
        }
        return config;
    }
}
