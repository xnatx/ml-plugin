/*
 * ml-plugin: org.nrg.xnatx.plugins.ml.rest.MlTrainingApi
 * XNAT http://www.xnat.org
 * Copyright (c) 2005-2021, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.xnatx.plugins.ml.rest;

import static lombok.AccessLevel.PROTECTED;
import static org.springframework.http.MediaType.*;
import static org.springframework.web.bind.annotation.RequestMethod.*;

import io.swagger.annotations.*;
import lombok.Getter;
import lombok.experimental.Accessors;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.nrg.framework.annotations.XapiRestController;
import org.nrg.io.xnat.XnatCatalogURIIterator;
import org.nrg.xapi.exceptions.DataFormatException;
import org.nrg.xapi.exceptions.InsufficientPrivilegesException;
import org.nrg.xapi.exceptions.NotFoundException;
import org.nrg.xapi.exceptions.ResourceAlreadyExistsException;
import org.nrg.xapi.rest.AbstractExperimentXapiRestController;
import org.nrg.xapi.rest.XapiRequestMapping;
import org.nrg.xdat.model.MlTrainsessionI;
import org.nrg.xdat.model.XnatAbstractresourceI;
import org.nrg.xdat.om.MlTrainsession;
import org.nrg.xdat.om.XnatResourcecatalog;
import org.nrg.xdat.security.services.RoleHolder;
import org.nrg.xdat.security.services.UserManagementServiceI;
import org.nrg.xnatx.plugins.ml.models.LaunchTrainingRequestData;
import org.nrg.xnatx.plugins.ml.services.MlModelService;
import org.nrg.xnatx.plugins.ml.services.MlProcessingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.InputStreamSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.web.bind.annotation.*;

import java.io.File;
import java.io.FileInputStream;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Api("Machine Learning Training Run API")
@XapiRestController
@RequestMapping("/ml/train")
@Getter(PROTECTED)
@Accessors(prefix = "_")
@Slf4j
public class MlTrainingApi extends AbstractExperimentXapiRestController<MlTrainsession> {
    @Autowired
    protected MlTrainingApi(final UserManagementServiceI userManagementService, final RoleHolder roleHolder, final NamedParameterJdbcTemplate template, final MlProcessingService service, final MlModelService models) throws IllegalAccessException, NoSuchFieldException {
        super(template, userManagementService, roleHolder);
        _service = service;
        _models = models;
    }

    @ApiOperation(value = "Get list of all training sessions accessible to the current user.", response = String.class, responseContainer = "Map")
    @ApiResponses({@ApiResponse(code = 200, message = "List of training sessions successfully retrieved."),
                   @ApiResponse(code = 401, message = "Must be authenticated to access the XNAT REST API."),
                   @ApiResponse(code = 403, message = "Not authorized to access this function."),
                   @ApiResponse(code = 500, message = "Unexpected error")})
    @XapiRequestMapping(produces = APPLICATION_JSON_VALUE, method = GET)
    @ResponseBody
    public Map<String, Map<String, String>> getAllTrainingSessions() {
        return getService().retrieveTrainingSessions(getSessionUser());
    }

    @ApiOperation(value = "Get list of training sessions for the specified project.", response = String.class, responseContainer = "Map")
    @ApiResponses({@ApiResponse(code = 200, message = "List of training sessions successfully retrieved."),
                   @ApiResponse(code = 401, message = "Must be authenticated to access the XNAT REST API."),
                   @ApiResponse(code = 403, message = "Not authorized to access this function."),
                   @ApiResponse(code = 404, message = "Couldn't find specified project."),
                   @ApiResponse(code = 500, message = "Unexpected error")})
    @XapiRequestMapping(value = "project/{projectId}", produces = APPLICATION_JSON_VALUE, method = GET)
    @ResponseBody
    public Map<String, Map<String, Object>> getTrainingSessions(final @PathVariable String projectId) throws NotFoundException, InsufficientPrivilegesException {
        return getService().retrieveTrainingSessions(getSessionUser(), projectId);
    }

    @ApiOperation(value = "Get map of training sessions organized by status for the specified project.", notes = "The value for status can include multiple statuses separated by commas, e.g. \".../status/failed,successful\".", response = String.class, responseContainer = "Map")
    @ApiResponses({@ApiResponse(code = 200, message = "List of training sessions successfully retrieved."),
                   @ApiResponse(code = 401, message = "Must be authenticated to access the XNAT REST API."),
                   @ApiResponse(code = 403, message = "Not authorized to access this function."),
                   @ApiResponse(code = 404, message = "Couldn't find specified project."),
                   @ApiResponse(code = 500, message = "Unexpected error")})
    @XapiRequestMapping(value = "project/{projectId}/status/{statuses}", produces = APPLICATION_JSON_VALUE, method = GET)
    @ResponseBody
    public Map<String, Map<String, Map<String, Object>>> getTrainingSessions(final @PathVariable String projectId, final @PathVariable String statuses) throws NotFoundException, InsufficientPrivilegesException {
        return getService().findSessionsByStatus(getSessionUser(), projectId, StringUtils.split(statuses, ","));
    }

    @ApiOperation(value = "Launch training session.", notes = "This call returns the ID of the training session instance. That ID can be used to retrieve the status of the training session.", response = String.class)
    @ApiResponses({@ApiResponse(code = 200, message = "Training session successfully launched."),
                   @ApiResponse(code = 401, message = "Must be authenticated to access the XNAT REST API."),
                   @ApiResponse(code = 403, message = "Not authorized to access this function."),
                   @ApiResponse(code = 404, message = "Couldn't find referenced training session."),
                   @ApiResponse(code = 500, message = "Unexpected error")})
    @XapiRequestMapping(value = "launch", consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE, method = POST)
    @ResponseBody
    public String launchTraining(final @RequestBody LaunchTrainingRequestData launchModel) throws NotFoundException, ResourceAlreadyExistsException, DataFormatException {
        return getService().launchTrainingSession(getSessionUser(), launchModel).getId();
    }

    @ApiOperation(value = "Queue an existing training session for execution.")
    @ApiResponses({@ApiResponse(code = 200, message = "Training session successfully queued."),
                   @ApiResponse(code = 401, message = "Must be authenticated to access the XNAT REST API."),
                   @ApiResponse(code = 403, message = "Not authorized to access this function."),
                   @ApiResponse(code = 404, message = "Couldn't find referenced training session."),
                   @ApiResponse(code = 500, message = "Unexpected error")})
    @XapiRequestMapping(value = "{sessionId}/queue", produces = APPLICATION_XML_VALUE, method = POST)
    @ResponseBody
    public MlTrainsessionI queueTraining(final @PathVariable String sessionId) throws NotFoundException, DataFormatException {
        return getService().queueTrainingSession(getSessionUser(), sessionId);
    }

    @ApiOperation(value = "Queue an existing training session for execution.")
    @ApiResponses({@ApiResponse(code = 200, message = "Training session successfully queued."),
                   @ApiResponse(code = 401, message = "Must be authenticated to access the XNAT REST API."),
                   @ApiResponse(code = 403, message = "Not authorized to access this function."),
                   @ApiResponse(code = 404, message = "Couldn't find referenced training session."),
                   @ApiResponse(code = 500, message = "Unexpected error")})
    @XapiRequestMapping(value = "project/{projectId}/{idOrLabel}/queue", produces = APPLICATION_XML_VALUE, method = POST)
    @ResponseBody
    public MlTrainsessionI queueTraining(final @PathVariable String projectId, final @PathVariable String idOrLabel) throws NotFoundException, DataFormatException {
        return getService().queueTrainingSession(getSessionUser(), projectId, idOrLabel);
    }

    @ApiOperation(value = "Annotate training session.", response = String.class)
    @ApiResponses({@ApiResponse(code = 200, message = "Training session successfully annotated."),
                   @ApiResponse(code = 401, message = "Must be authenticated to access the XNAT REST API."),
                   @ApiResponse(code = 403, message = "Not authorized to access this function."),
                   @ApiResponse(code = 404, message = "Couldn't find referenced training session."),
                   @ApiResponse(code = 500, message = "Unexpected error")})
    @XapiRequestMapping(value = "{sessionId}/annotate", produces = APPLICATION_JSON_VALUE, method = GET)
    @ResponseBody
    public String getTrainingSessionAnnotation(final @PathVariable String sessionId) throws NotFoundException {
        return getService().getSessionAnnotation(getSessionUser(), sessionId);
    }

    @ApiOperation(value = "Annotate training session.", response = String.class)
    @ApiResponses({@ApiResponse(code = 200, message = "Training session successfully annotated."),
                   @ApiResponse(code = 401, message = "Must be authenticated to access the XNAT REST API."),
                   @ApiResponse(code = 403, message = "Not authorized to access this function."),
                   @ApiResponse(code = 404, message = "Couldn't find referenced training session."),
                   @ApiResponse(code = 500, message = "Unexpected error")})
    @XapiRequestMapping(value = "{sessionId}/annotate", produces = APPLICATION_JSON_VALUE, method = PUT)
    @ResponseBody
    public void annotateTrainingSession(final @PathVariable String sessionId, final @RequestBody String notes)
            throws NotFoundException, DataFormatException {
        getService().annotateSession(getSessionUser(), sessionId, notes);
    }

    @ApiOperation(value = "Annotate training session.", response = String.class)
    @ApiResponses({@ApiResponse(code = 200, message = "Training session successfully annotated."),
                   @ApiResponse(code = 401, message = "Must be authenticated to access the XNAT REST API."),
                   @ApiResponse(code = 403, message = "Not authorized to access this function."),
                   @ApiResponse(code = 404, message = "Couldn't find referenced training session."),
                   @ApiResponse(code = 500, message = "Unexpected error")})
    @XapiRequestMapping(value = "project/{projectId}/{idOrLabel}/annotate", produces = APPLICATION_JSON_VALUE, method = GET)
    @ResponseBody
    public String getTrainingSessionAnnotation(final @PathVariable String projectId, final @PathVariable String idOrLabel) throws NotFoundException {
        return getService().getSessionAnnotation(getSessionUser(), projectId, idOrLabel);
    }

    @ApiOperation(value = "Annotate training session.", response = String.class)
    @ApiResponses({@ApiResponse(code = 200, message = "Training session successfully annotated."),
                   @ApiResponse(code = 401, message = "Must be authenticated to access the XNAT REST API."),
                   @ApiResponse(code = 403, message = "Not authorized to access this function."),
                   @ApiResponse(code = 404, message = "Couldn't find referenced training session."),
                   @ApiResponse(code = 500, message = "Unexpected error")})
    @XapiRequestMapping(value = "project/{projectId}/{idOrLabel}/annotate", produces = APPLICATION_JSON_VALUE, method = PUT)
    @ResponseBody
    public void annotateTrainingSession(final @PathVariable String projectId, final @PathVariable String idOrLabel, final @RequestBody String notes)
            throws NotFoundException, DataFormatException {
        getService().annotateSession(getSessionUser(), projectId, idOrLabel, notes);
    }

    @ApiOperation(value = "Get training session.", notes = "This returns the full XML-based Machine Learning training session object, which includes references to the input objects (i.e. training configuration, data collection, model), as well as the list of parameters provided for this session.", response = MlTrainsessionI.class)
    @ApiResponses({@ApiResponse(code = 200, message = "Training session successfully retrieved."),
                   @ApiResponse(code = 401, message = "Must be authenticated to access the XNAT REST API."),
                   @ApiResponse(code = 403, message = "Not authorized to access this function."),
                   @ApiResponse(code = 404, message = "Couldn't find referenced training session."),
                   @ApiResponse(code = 500, message = "Unexpected error")})
    @XapiRequestMapping(value = "{sessionId}", produces = APPLICATION_XML_VALUE, method = GET)
    @ResponseBody
    public MlTrainsessionI getTrainingSession(final @PathVariable String sessionId) throws NotFoundException {
        return getService().retrieveTrainingSession(getSessionUser(), sessionId);
    }

    @ApiOperation(value = "Get training session.", notes = "This returns the full XML-based Machine Learning training session object, which includes references to the input objects (i.e. training configuration, data collection, model), as well as the list of parameters provided for this session.", response = MlTrainsessionI.class)
    @ApiResponses({@ApiResponse(code = 200, message = "Training session successfully retrieved."),
                   @ApiResponse(code = 401, message = "Must be authenticated to access the XNAT REST API."),
                   @ApiResponse(code = 403, message = "Not authorized to access this function."),
                   @ApiResponse(code = 404, message = "Couldn't find referenced training session."),
                   @ApiResponse(code = 500, message = "Unexpected error")})
    @XapiRequestMapping(value = "project/{projectId}/{idOrLabel}", produces = APPLICATION_XML_VALUE, method = GET)
    @ResponseBody
    public MlTrainsessionI getTrainingSession(final @PathVariable String projectId, final @PathVariable String idOrLabel) throws NotFoundException {
        return getService().retrieveTrainingSession(getSessionUser(), projectId, idOrLabel);
    }

    @ApiOperation(value = "Update training session.", response = MlTrainsessionI.class)
    @ApiResponses({@ApiResponse(code = 200, message = "Training session successfully updated."),
                   @ApiResponse(code = 401, message = "Must be authenticated to access the XNAT REST API."),
                   @ApiResponse(code = 403, message = "Not authorized to access this function."),
                   @ApiResponse(code = 404, message = "Couldn't find referenced training session."),
                   @ApiResponse(code = 500, message = "Unexpected error")})
    @XapiRequestMapping(value = "{sessionId}", consumes = APPLICATION_XML_VALUE, produces = APPLICATION_XML_VALUE, method = PUT)
    @ResponseBody
    public MlTrainsessionI updateTrainingSession(final @PathVariable String sessionId, final @RequestBody MlTrainsessionI session) throws DataFormatException, NotFoundException {
        MlTrainsession mlTrainsession = (MlTrainsession) session;
        validateEntityId(sessionId, mlTrainsession);
        return getService().updateTrainingSession(getSessionUser(), mlTrainsession);
    }

    @ApiOperation(value = "Update training session.", response = MlTrainsessionI.class)
    @ApiResponses({@ApiResponse(code = 200, message = "Training session successfully updated."),
                   @ApiResponse(code = 401, message = "Must be authenticated to access the XNAT REST API."),
                   @ApiResponse(code = 403, message = "Not authorized to access this function."),
                   @ApiResponse(code = 404, message = "Couldn't find referenced training session."),
                   @ApiResponse(code = 500, message = "Unexpected error")})
    @XapiRequestMapping(value = "project/{projectId}/{idOrLabel}", consumes = APPLICATION_XML_VALUE, produces = APPLICATION_XML_VALUE, method = PUT)
    @ResponseBody
    public MlTrainsessionI updateTrainingSession(final @PathVariable String projectId, final @PathVariable String idOrLabel, final @RequestBody MlTrainsessionI session) throws DataFormatException, NotFoundException {
        MlTrainsession mlTrainsession = (MlTrainsession) session;
        validateEntityId(projectId, idOrLabel, mlTrainsession);
        return getService().updateTrainingSession(getSessionUser(), mlTrainsession);
    }

    @ApiOperation(value = "Update training session.", response = MlTrainsessionI.class)
    @ApiResponses({@ApiResponse(code = 200, message = "Training session successfully updated."),
                   @ApiResponse(code = 401, message = "Must be authenticated to access the XNAT REST API."),
                   @ApiResponse(code = 403, message = "Not authorized to access this function."),
                   @ApiResponse(code = 404, message = "Couldn't find referenced training session."),
                   @ApiResponse(code = 500, message = "Unexpected error")})
    @XapiRequestMapping(value = "{sessionId}", consumes = MULTIPART_FORM_DATA_VALUE, produces = APPLICATION_XML_VALUE, method = PUT)
    @ResponseBody
    public MlTrainsessionI updateTrainingSession(final @PathVariable String sessionId, final @RequestParam MlTrainsessionI session, final @RequestParam Map<String, String> parameters) throws DataFormatException, NotFoundException {
        MlTrainsession mlTrainsession = (MlTrainsession) session;
        validateEntityId(sessionId, mlTrainsession);
        return getService().updateTrainingSession(getSessionUser(), mlTrainsession, parameters);
    }

    @ApiOperation(value = "Update training session.", response = MlTrainsessionI.class)
    @ApiResponses({@ApiResponse(code = 200, message = "Training session successfully updated."),
                   @ApiResponse(code = 401, message = "Must be authenticated to access the XNAT REST API."),
                   @ApiResponse(code = 403, message = "Not authorized to access this function."),
                   @ApiResponse(code = 404, message = "Couldn't find referenced training session."),
                   @ApiResponse(code = 500, message = "Unexpected error")})
    @XapiRequestMapping(value = "project/{projectId}/{idOrLabel}", consumes = MULTIPART_FORM_DATA_VALUE, produces = APPLICATION_XML_VALUE, method = PUT)
    @ResponseBody
    public MlTrainsessionI updateTrainingSession(final @PathVariable String projectId, final @PathVariable String idOrLabel, final @RequestParam MlTrainsessionI session, final @RequestParam Map<String, String> parameters) throws DataFormatException, NotFoundException {
        MlTrainsession mlTrainsession = (MlTrainsession) session;
        validateEntityId(projectId, idOrLabel, mlTrainsession);
        return getService().updateTrainingSession(getSessionUser(), mlTrainsession, parameters);
    }

    @ApiOperation(value = "Update training session.", response = MlTrainsessionI.class)
    @ApiResponses({@ApiResponse(code = 200, message = "Training session successfully updated."),
                   @ApiResponse(code = 401, message = "Must be authenticated to access the XNAT REST API."),
                   @ApiResponse(code = 403, message = "Not authorized to access this function."),
                   @ApiResponse(code = 404, message = "Couldn't find referenced training session."),
                   @ApiResponse(code = 500, message = "Unexpected error")})
    @XapiRequestMapping(value = "{sessionId}", consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_XML_VALUE, method = PUT)
    @ResponseBody
    public MlTrainsessionI updateTrainingSession(final @PathVariable String sessionId, final @RequestBody Map<String, String> parameters) throws NotFoundException, DataFormatException {
        validateId(sessionId);
        return getService().updateTrainingSession(getSessionUser(), getService().retrieveTrainingSession(getSessionUser(), sessionId), parameters);
    }

    @ApiOperation(value = "Update training session.", response = MlTrainsessionI.class)
    @ApiResponses({@ApiResponse(code = 200, message = "Training session successfully updated."),
                   @ApiResponse(code = 401, message = "Must be authenticated to access the XNAT REST API."),
                   @ApiResponse(code = 403, message = "Not authorized to access this function."),
                   @ApiResponse(code = 404, message = "Couldn't find referenced training session."),
                   @ApiResponse(code = 500, message = "Unexpected error")})
    @XapiRequestMapping(value = "project/{projectId}/{idOrLabel}", consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_XML_VALUE, method = PUT)
    @ResponseBody
    public MlTrainsessionI updateTrainingSession(final @PathVariable String projectId, final @PathVariable String idOrLabel, final @RequestBody Map<String, String> parameters) throws NotFoundException, DataFormatException {
        validateId(projectId, idOrLabel);
        return getService().updateTrainingSession(getSessionUser(), getService().retrieveTrainingSession(getSessionUser(), projectId, idOrLabel), parameters);
    }

    @ApiOperation(value = "Delete training session.")
    @ApiResponses({@ApiResponse(code = 200, message = "Training session successfully deleted."),
                   @ApiResponse(code = 401, message = "Must be authenticated to access the XNAT REST API."),
                   @ApiResponse(code = 403, message = "Not authorized to access this function."),
                   @ApiResponse(code = 404, message = "Couldn't find referenced training session."),
                   @ApiResponse(code = 500, message = "Unexpected error")})
    @XapiRequestMapping(value = "{sessionId}", produces = APPLICATION_JSON_VALUE, method = DELETE)
    public void deleteTrainingSession(final @PathVariable String sessionId) throws NotFoundException {
        getService().deleteTrainingSession(getSessionUser(), sessionId);
    }

    @ApiOperation(value = "Delete training session.")
    @ApiResponses({@ApiResponse(code = 200, message = "Training session successfully deleted."),
                   @ApiResponse(code = 401, message = "Must be authenticated to access the XNAT REST API."),
                   @ApiResponse(code = 403, message = "Not authorized to access this function."),
                   @ApiResponse(code = 404, message = "Couldn't find referenced training session."),
                   @ApiResponse(code = 500, message = "Unexpected error")})
    @XapiRequestMapping(value = "project/{projectId}/{idOrLabel}", produces = APPLICATION_JSON_VALUE, method = DELETE)
    public void deleteTrainingSession(final @PathVariable String projectId, final @PathVariable String idOrLabel) throws NotFoundException {
        getService().deleteTrainingSession(getSessionUser(), projectId, idOrLabel);
    }

    @ApiOperation(value = "Create a new model from an existing training session.")
    @ApiResponses({@ApiResponse(code = 200, message = "Training model successfully created."),
                   @ApiResponse(code = 401, message = "Must be authenticated to access the XNAT REST API."),
                   @ApiResponse(code = 403, message = "Not authorized to access this function."),
                   @ApiResponse(code = 404, message = "Couldn't find referenced training session."),
                   @ApiResponse(code = 500, message = "Unexpected error")})
    @XapiRequestMapping(value = "{sessionId}/createModel/{projectId}/{label}", method = POST)
    @ResponseBody
    public String createModel(final @PathVariable String sessionId, final @PathVariable String projectId, final @PathVariable String label,
                              @ApiParam("Indicates whether the best model should be used if it is different than the final model") @RequestParam(name = "useBestModel", required = false, defaultValue = "false") final boolean useBestModel) throws Exception {
        final MlTrainsession              existingSession = getService().retrieveTrainingSession(getSessionUser(), sessionId);
        final Map<String, InputStreamSource> sources         = new HashMap<>();
        final List<XnatAbstractresourceI>    results         = existingSession.getSessionResults();

        final List<String> urisToCopy  = new ArrayList<>();
        String             resultsUri  = ((XnatResourcecatalog) results.get(1)).getUri();
        File               resultsFile = new File(resultsUri);

        if (useBestModel) {
            final XnatCatalogURIIterator resultFilesIterator = new XnatCatalogURIIterator(resultsFile);
            while (resultFilesIterator.hasNext()) {
                final String filename = getLastBitFromUri(resultFilesIterator.next().getPath());
                if (filename.startsWith("model.ckpt")) {
                    urisToCopy.add(filename);
                }
            }
        }
        if (urisToCopy.isEmpty()) {
            //Use final model files unless user requested to use the best model and best model files exist
            final XnatCatalogURIIterator resultFilesIterator = new XnatCatalogURIIterator(resultsFile);
            while (resultFilesIterator.hasNext()) {
                final String filename = getLastBitFromUri(resultFilesIterator.next().getPath());
                if (filename.startsWith("model_final.ckpt")) {
                    urisToCopy.add(filename);
                }
            }
        }
        final String catalogFilename  = getLastBitFromUri(resultsUri);
        final String catalogDirectory = resultsUri.substring(0, resultsUri.length() - catalogFilename.length());
        for (final String filename : urisToCopy) {
            String stringPathToCopy = Paths.get(catalogDirectory, filename).toString();
            File   fileToCopy       = new File(stringPathToCopy);
            //Remove the _final in the filenames if it exists
            String newFilename = filename.replace("model_final", "model");
            sources.put(newFilename, new InputStreamResource(new FileInputStream(fileToCopy)));
        }

        return _models.createMlModel(getSessionUser(), projectId, label, sources).getId();
    }

    private static String getLastBitFromUri(final String url) {
        return url.replaceFirst(".*/([^/?]+).*", "$1");
    }

    private final MlProcessingService _service;
    private final MlModelService      _models;
}
