/*
 * ml-plugin: org.nrg.xnatx.plugins.ml.rest.MlPrefsApi
 * XNAT http://www.xnat.org
 * Copyright (c) 2005-2021, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.xnatx.plugins.ml.rest;

import static org.nrg.framework.exceptions.NrgServiceError.ConfigurationError;
import static org.nrg.xdat.security.helpers.AccessLevel.Authenticated;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.util.Map;
import lombok.extern.slf4j.Slf4j;
import org.nrg.framework.annotations.XapiRestController;
import org.nrg.framework.exceptions.NrgServiceException;
import org.nrg.prefs.exceptions.InvalidPreferenceName;
import org.nrg.xapi.exceptions.NotFoundException;
import org.nrg.xapi.rest.AbstractXapiRestController;
import org.nrg.xapi.rest.XapiRequestMapping;
import org.nrg.xdat.security.services.RoleHolder;
import org.nrg.xdat.security.services.UserManagementServiceI;
import org.nrg.xnatx.plugins.ml.preferences.MlPreferencesBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Api("Machine Learning Preferences API")
@XapiRestController
@RequestMapping("/ml/prefs")
@Slf4j
public class MlPrefsApi extends AbstractXapiRestController {
    @Autowired
    public MlPrefsApi(final UserManagementServiceI userManagementService, final RoleHolder roleHolder, final MlPreferencesBean preferences) {
        super(userManagementService, roleHolder);
        _preferences = preferences;
    }

    @ApiOperation(value = "Returns the full map of Machine Learning preferences.", response = String.class, responseContainer = "Map")
    @ApiResponses({@ApiResponse(code = 200, message = "Site configuration properties successfully retrieved."),
                   @ApiResponse(code = 401, message = "Must be authenticated to access the XNAT REST API."),
                   @ApiResponse(code = 403, message = "Not authorized to set site configuration properties."),
                   @ApiResponse(code = 500, message = "Unexpected error")})
    @XapiRequestMapping(produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.GET, restrictTo = Authenticated)
    public Map<String, Object> getPreferences() {
        return _preferences;
    }

    @ApiOperation(value = "Returns the value of the specified Machine Learning preference.", response = String.class)
    @ApiResponses({@ApiResponse(code = 200, message = "Machine Learning preference value successfully retrieved."),
                   @ApiResponse(code = 401, message = "Must be authenticated to access the XNAT REST API."),
                   @ApiResponse(code = 403, message = "Not authorized to access Machine Learning preferences."),
                   @ApiResponse(code = 500, message = "Unexpected error")})
    @XapiRequestMapping(value = "{preference}", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.GET, restrictTo = Authenticated)
    public String getPreferenceValue(@ApiParam(value = "The Machine Learning preference to retrieve.", required = true) @PathVariable final String preference) throws NotFoundException {
        if (!_preferences.containsKey(preference)) {
            throw new NotFoundException("No preference named \"" + preference + "\" was found.");
        }
        return _preferences.getValue(preference);
    }

    @ApiOperation(value = "Updates the value of the specified Machine Learning preference.", response = String.class)
    @ApiResponses({@ApiResponse(code = 200, message = "Machine Learning preference value successfully retrieved."),
                   @ApiResponse(code = 401, message = "Must be authenticated to access the XNAT REST API."),
                   @ApiResponse(code = 403, message = "Not authorized to access Machine Learning preferences."),
                   @ApiResponse(code = 500, message = "Unexpected error")})
    @XapiRequestMapping(value = "{preference}", consumes = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.PUT, restrictTo = Authenticated)
    public String setPreferenceValue(@ApiParam(value = "The Machine Learning preference to set.", required = true) @PathVariable final String preference,
                                     @ApiParam(value = "The Machine Learning preference to set.", required = true) @RequestBody final String value) throws NotFoundException, NrgServiceException {
        if (!_preferences.containsKey(preference)) {
            throw new NotFoundException("No preference named \"" + preference + "\" was found.");
        }
        try {
            return _preferences.set(value, preference);
        } catch (InvalidPreferenceName invalidPreferenceName) {
            throw new NrgServiceException(ConfigurationError, "An error occurred trying to set the \"" + preference + "\" Machine Learning preference to the value: " + value);
        }
    }

    private final MlPreferencesBean _preferences;
}
