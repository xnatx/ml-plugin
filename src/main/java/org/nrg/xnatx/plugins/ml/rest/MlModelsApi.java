/*
 * ml-plugin: org.nrg.xnatx.plugins.ml.rest.MlModelsApi
 * XNAT http://www.xnat.org
 * Copyright (c) 2005-2021, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.xnatx.plugins.ml.rest;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.Getter;
import lombok.experimental.Accessors;
import lombok.extern.slf4j.Slf4j;
import org.nrg.action.ServerException;
import org.nrg.framework.annotations.XapiRestController;
import org.nrg.xapi.exceptions.DataFormatException;
import org.nrg.xapi.exceptions.InsufficientPrivilegesException;
import org.nrg.xapi.exceptions.NotFoundException;
import org.nrg.xapi.rest.AbstractExperimentXapiRestController;
import org.nrg.xapi.rest.XapiRequestMapping;
import org.nrg.xdat.model.MlModelI;
import org.nrg.xdat.om.MlModel;
import org.nrg.xdat.om.XnatResourcecatalog;
import org.nrg.xdat.preferences.SiteConfigPreferences;
import org.nrg.xdat.security.helpers.AccessLevel;
import org.nrg.xdat.security.services.RoleHolder;
import org.nrg.xdat.security.services.UserManagementServiceI;
import org.nrg.xnat.utils.CatalogUtils;
import org.nrg.xnat.web.http.AbstractZipStreamingResponseBody;
import org.nrg.xnat.web.http.CatalogZipStreamingResponseBody;
import org.nrg.xnatx.plugins.ml.services.MlModelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.InputStreamSource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.method.annotation.StreamingResponseBody;

import javax.annotation.Nonnull;
import java.io.File;
import java.util.Arrays;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.toMap;
import static lombok.AccessLevel.PROTECTED;
import static org.springframework.http.MediaType.*;
import static org.springframework.web.bind.annotation.RequestMethod.*;

@Api("Machine Learning Model API")
@XapiRestController
@RequestMapping(value = "/ml/models")
@Getter(PROTECTED)
@Accessors(prefix = "_")
@Slf4j
public class MlModelsApi extends AbstractExperimentXapiRestController<MlModel> {
    @Autowired
    protected MlModelsApi(final UserManagementServiceI userManagementService, final RoleHolder roleHolder, final NamedParameterJdbcTemplate template, final SiteConfigPreferences preferences, final MlModelService service) throws IllegalAccessException, NoSuchFieldException {
        super(template, userManagementService, roleHolder);
        _archivePath = preferences.getArchivePath();
        _service = service;
    }

    @ApiOperation(value = "Get list of models in the specified project.", notes = "The return value is a map of model IDs along with the label for the model in the specified project.", response = String.class, responseContainer = "Map")
    @ApiResponses({@ApiResponse(code = 200, message = "List of models successfully retrieved."),
                   @ApiResponse(code = 401, message = "Must be authenticated to access the XNAT REST API."),
                   @ApiResponse(code = 403, message = "Not authorized to access this function."),
                   @ApiResponse(code = 404, message = "Couldn't find referenced model."),
                   @ApiResponse(code = 500, message = "Unexpected error")})
    @XapiRequestMapping(value = "{projectId}", produces = APPLICATION_JSON_VALUE, method = GET, restrictTo = AccessLevel.Read)
    @ResponseBody
    public Map<String, String> getModels(final @PathVariable String projectId) throws NotFoundException, InsufficientPrivilegesException {
        return getService().retrieveMlModels(getSessionUser(), projectId);
    }

    @ApiOperation(value = "Create project model.", response = MlModelI.class)
    @ApiResponses({@ApiResponse(code = 200, message = "Training model successfully created."),
                   @ApiResponse(code = 401, message = "Must be authenticated to access the XNAT REST API."),
                   @ApiResponse(code = 403, message = "Not authorized to access this function."),
                   @ApiResponse(code = 404, message = "Couldn't find referenced model."),
                   @ApiResponse(code = 500, message = "Unexpected error")})
    @XapiRequestMapping(value = "model/{projectId}/{label}", produces = APPLICATION_XML_VALUE, method = POST)
    @ResponseBody
    public String createModel(final @PathVariable String projectId, final @PathVariable String label) {
        return getService().createMlModel(getSessionUser(), projectId, label).getId();
    }

    @ApiOperation(value = "Create project model.", response = MlModelI.class)
    @ApiResponses({@ApiResponse(code = 200, message = "Training model successfully created."),
                   @ApiResponse(code = 401, message = "Must be authenticated to access the XNAT REST API."),
                   @ApiResponse(code = 403, message = "Not authorized to access this function."),
                   @ApiResponse(code = 404, message = "Couldn't find referenced model."),
                   @ApiResponse(code = 500, message = "Unexpected error")})
    @XapiRequestMapping(value = "model/{projectId}/{label}", consumes = MULTIPART_FORM_DATA_VALUE, produces = APPLICATION_XML_VALUE, method = POST)
    @ResponseBody
    public String createModel(final @PathVariable String projectId, final @PathVariable String label, final @RequestParam("modelFile") MultipartFile[] modelFile) {
        return getService().createMlModel(getSessionUser(), projectId, label, getSourcesForMultipartFiles(modelFile)).getId();
    }

    @ApiOperation(value = "Create project model with resources from uploaded zip file.", response = MlModelI.class)
    @ApiResponses({@ApiResponse(code = 200, message = "Training model successfully created."),
                   @ApiResponse(code = 401, message = "Must be authenticated to access the XNAT REST API."),
                   @ApiResponse(code = 403, message = "Not authorized to access this function."),
                   @ApiResponse(code = 404, message = "Couldn't find referenced model."),
                   @ApiResponse(code = 500, message = "Unexpected error")})
    @XapiRequestMapping(value = "model/{projectId}/{label}", consumes = "application/zip", produces = APPLICATION_XML_VALUE, method = POST)
    @ResponseBody
    public String createModel(final @PathVariable String projectId, final @PathVariable String label, @RequestBody final Map<String, File> modelFiles) {
        return getService().createMlModel(getSessionUser(), projectId, label, getModelFileResourceMap(modelFiles)).getId();
    }

    @ApiOperation(value = "Get model with the indicated ID.", response = MlModelI.class)
    @ApiResponses({@ApiResponse(code = 200, message = "Model successfully retrieved."),
                   @ApiResponse(code = 401, message = "Must be authenticated to access the XNAT REST API."),
                   @ApiResponse(code = 403, message = "Not authorized to access this function."),
                   @ApiResponse(code = 404, message = "Couldn't find referenced model."),
                   @ApiResponse(code = 500, message = "Unexpected error")})
    @XapiRequestMapping(value = "model/{modelId}", produces = APPLICATION_XML_VALUE, method = GET)
    @ResponseBody
    public MlModelI getModel(final @PathVariable String modelId) throws NotFoundException {
        return getService().retrieveMlModel(getSessionUser(), modelId);
    }

    @ApiOperation(value = "Get model with the specified ID in the project.", response = MlModelI.class)
    @ApiResponses({@ApiResponse(code = 200, message = "Model successfully retrieved."),
                   @ApiResponse(code = 401, message = "Must be authenticated to access the XNAT REST API."),
                   @ApiResponse(code = 403, message = "Not authorized to access this function."),
                   @ApiResponse(code = 404, message = "Couldn't find referenced model."),
                   @ApiResponse(code = 500, message = "Unexpected error")})
    @XapiRequestMapping(value = "model/{projectId}/{idOrLabel}", produces = APPLICATION_XML_VALUE, method = GET)
    @ResponseBody
    public MlModelI getModel(final @PathVariable String projectId, final @PathVariable String idOrLabel) throws NotFoundException {
        return getService().retrieveMlModel(getSessionUser(), projectId, idOrLabel);
    }

    @ApiOperation(value = "Get data for the model with the indicated ID.", response = StreamingResponseBody.class)
    @ApiResponses({@ApiResponse(code = 200, message = "Model successfully retrieved."),
                   @ApiResponse(code = 401, message = "Must be authenticated to access the XNAT REST API."),
                   @ApiResponse(code = 403, message = "Not authorized to access this function."),
                   @ApiResponse(code = 404, message = "Couldn't find referenced model."),
                   @ApiResponse(code = 500, message = "Unexpected error")})
    @XapiRequestMapping(value = "data/{modelId}", produces = APPLICATION_XML_VALUE, method = GET)
    @ResponseBody
    public ResponseEntity<StreamingResponseBody> getModelData(final @PathVariable String modelId) throws NotFoundException {
        final MlModel model = getService().retrieveMlModel(getSessionUser(), modelId);
        return getStreamingModelData(model);
    }

    @ApiOperation(value = "Get data for the model with the specified ID in the project.", response = StreamingResponseBody.class)
    @ApiResponses({@ApiResponse(code = 200, message = "Model successfully retrieved."),
                   @ApiResponse(code = 401, message = "Must be authenticated to access the XNAT REST API."),
                   @ApiResponse(code = 403, message = "Not authorized to access this function."),
                   @ApiResponse(code = 404, message = "Couldn't find referenced model."),
                   @ApiResponse(code = 500, message = "Unexpected error")})
    @XapiRequestMapping(value = "data/{projectId}/{idOrLabel}", produces = APPLICATION_XML_VALUE, method = GET)
    @ResponseBody
    public ResponseEntity<StreamingResponseBody> getModelData(final @PathVariable String projectId, final @PathVariable String idOrLabel) throws NotFoundException {
        return getStreamingModelData(getService().retrieveMlModel(getSessionUser(), projectId, idOrLabel));
    }

    @ApiOperation(value = "Update model.", response = MlModelI.class)
    @ApiResponses({@ApiResponse(code = 200, message = "Model successfully updated."),
                   @ApiResponse(code = 401, message = "Must be authenticated to access the XNAT REST API."),
                   @ApiResponse(code = 403, message = "Not authorized to access this function."),
                   @ApiResponse(code = 404, message = "Couldn't find referenced model."),
                   @ApiResponse(code = 500, message = "Unexpected error")})
    @XapiRequestMapping(value = "model/{modelId}", consumes = APPLICATION_XML_VALUE, produces = APPLICATION_XML_VALUE, method = PUT)
    @ResponseBody
    public MlModelI updateModel(final @PathVariable String modelId, final @RequestBody MlModelI model) throws DataFormatException, NotFoundException {
        MlModel mlModel = (MlModel) model;
        validateEntityId(modelId, mlModel);
        return getService().updateMlModel(getSessionUser(), mlModel);
    }

    @ApiOperation(value = "Update model.", response = MlModelI.class)
    @ApiResponses({@ApiResponse(code = 200, message = "Model successfully updated."),
                   @ApiResponse(code = 401, message = "Must be authenticated to access the XNAT REST API."),
                   @ApiResponse(code = 403, message = "Not authorized to access this function."),
                   @ApiResponse(code = 404, message = "Couldn't find referenced model."),
                   @ApiResponse(code = 500, message = "Unexpected error")})
    @XapiRequestMapping(value = "model/{projectId}/{idOrLabel}", consumes = APPLICATION_XML_VALUE, produces = APPLICATION_XML_VALUE, method = PUT)
    @ResponseBody
    public MlModelI updateModel(final @PathVariable String projectId, final @PathVariable String idOrLabel, final @RequestBody MlModelI model) throws DataFormatException, NotFoundException {
        MlModel mlModel = (MlModel) model;
        validateEntityId(projectId, idOrLabel, mlModel);
        return getService().updateMlModel(getSessionUser(), mlModel);
    }

    @ApiOperation(value = "Update data for the specified model.", response = MlModelI.class)
    @ApiResponses({@ApiResponse(code = 200, message = "Model successfully updated."),
                   @ApiResponse(code = 401, message = "Must be authenticated to access the XNAT REST API."),
                   @ApiResponse(code = 403, message = "Not authorized to access this function."),
                   @ApiResponse(code = 404, message = "Couldn't find referenced model."),
                   @ApiResponse(code = 500, message = "Unexpected error")})
    @XapiRequestMapping(value = "model/{modelId}", consumes = MULTIPART_FORM_DATA_VALUE, produces = APPLICATION_XML_VALUE, method = PUT)
    @ResponseBody
    public MlModelI updateModel(final @PathVariable String modelId, final @RequestParam("modelFile") MultipartFile[] modelFile) throws NotFoundException {
        validateId(modelId);
        return getService().updateMlModel(getSessionUser(), modelId, getSourcesForMultipartFiles(modelFile));
    }

    @ApiOperation(value = "Update data for the specified model.", response = MlModelI.class)
    @ApiResponses({@ApiResponse(code = 200, message = "Model successfully updated."),
                   @ApiResponse(code = 401, message = "Must be authenticated to access the XNAT REST API."),
                   @ApiResponse(code = 403, message = "Not authorized to access this function."),
                   @ApiResponse(code = 404, message = "Couldn't find referenced model."),
                   @ApiResponse(code = 500, message = "Unexpected error")})
    @XapiRequestMapping(value = "model/{modelId}", consumes = "application/zip", produces = APPLICATION_XML_VALUE, method = PUT)
    @ResponseBody
    public MlModelI updateModel(final @PathVariable String modelId, @RequestBody final Map<String, File> modelFiles) throws NotFoundException {
        validateId(modelId);
        return getService().updateMlModel(getSessionUser(), modelId, getModelFileResourceMap(modelFiles));
    }

    @ApiOperation(value = "Update data for the specified model.", response = MlModelI.class)
    @ApiResponses({@ApiResponse(code = 200, message = "Model successfully updated."),
                   @ApiResponse(code = 401, message = "Must be authenticated to access the XNAT REST API."),
                   @ApiResponse(code = 403, message = "Not authorized to access this function."),
                   @ApiResponse(code = 404, message = "Couldn't find referenced model."),
                   @ApiResponse(code = 500, message = "Unexpected error")})
    @XapiRequestMapping(value = "model/{projectId}/{idOrLabel}", consumes = MULTIPART_FORM_DATA_VALUE, produces = APPLICATION_XML_VALUE, method = PUT)
    @ResponseBody
    public MlModelI updateModel(final @PathVariable String projectId, final @PathVariable String idOrLabel, final @RequestParam("modelFile") MultipartFile[] modelFile) throws NotFoundException {
        validateId(projectId, idOrLabel);
        return getService().updateMlModel(getSessionUser(), projectId, idOrLabel, getSourcesForMultipartFiles(modelFile));
    }

    @ApiOperation(value = "Update data for the specified model.", response = MlModelI.class)
    @ApiResponses({@ApiResponse(code = 200, message = "Model successfully updated."),
                   @ApiResponse(code = 401, message = "Must be authenticated to access the XNAT REST API."),
                   @ApiResponse(code = 403, message = "Not authorized to access this function."),
                   @ApiResponse(code = 404, message = "Couldn't find referenced model."),
                   @ApiResponse(code = 500, message = "Unexpected error")})
    @XapiRequestMapping(value = "model/{projectId}/{idOrLabel}", consumes = "application/zip", produces = APPLICATION_XML_VALUE, method = PUT)
    @ResponseBody
    public MlModelI updateModel(final @PathVariable String projectId, final @PathVariable String idOrLabel, @RequestBody final Map<String, File> modelFiles) throws NotFoundException {
        validateId(projectId, idOrLabel);
        return getService().updateMlModel(getSessionUser(), projectId, idOrLabel, getModelFileResourceMap(modelFiles));
    }

    @ApiOperation(value = "Delete model with the specified ID.")
    @ApiResponses({@ApiResponse(code = 200, message = "Model successfully deleted."),
                   @ApiResponse(code = 401, message = "Must be authenticated to access the XNAT REST API."),
                   @ApiResponse(code = 403, message = "Not authorized to access this function."),
                   @ApiResponse(code = 404, message = "Couldn't find referenced model."),
                   @ApiResponse(code = 500, message = "Unexpected error")})
    @XapiRequestMapping(value = "model/{modelId}", method = DELETE)
    public void deleteModel(final @PathVariable String modelId) throws NotFoundException {
        getService().deleteMlModel(getSessionUser(), modelId);
    }

    @ApiOperation(value = "Delete model with the specified ID in the project.")
    @ApiResponses({@ApiResponse(code = 200, message = "Training model successfully deleted."),
                   @ApiResponse(code = 401, message = "Must be authenticated to access the XNAT REST API."),
                   @ApiResponse(code = 403, message = "Not authorized to access this function."),
                   @ApiResponse(code = 404, message = "Couldn't find referenced model."),
                   @ApiResponse(code = 500, message = "Unexpected error")})
    @XapiRequestMapping(value = "model/{projectId}/{idOrLabel}", method = DELETE)
    public void deleteModel(final @PathVariable String projectId, final @PathVariable String idOrLabel) throws NotFoundException {
        getService().deleteMlModel(getSessionUser(), projectId, idOrLabel);
    }

    @Nonnull
    private Map<String, FileSystemResource> getModelFileResourceMap(@RequestBody final Map<String, File> modelFiles) {
        return modelFiles.keySet().stream().collect(toMap(Function.identity(), name -> new FileSystemResource(modelFiles.get(name))));
    }

    private Map<String, ? extends InputStreamSource> getSourcesForMultipartFiles(final MultipartFile... files) {
        return Arrays.stream(files).collect(Collectors.toMap(MultipartFile::getOriginalFilename, Function.identity()));
    }

    private ResponseEntity<StreamingResponseBody> getStreamingModelData(final MlModel model) throws NotFoundException {
        final XnatResourcecatalog resourceCatalog = model.getModelResource(getSessionUser());
        if (resourceCatalog == null) {
            throw new NotFoundException("Didn't find any resources for model \"" + model.getLabel() + "\" in project " + model.getProject());
        }
        try {
            final CatalogUtils.CatalogData catalog = CatalogUtils.CatalogData.getOrCreate(_archivePath, resourceCatalog, model.getProject());
            return ResponseEntity.ok()
                                 .header(HttpHeaders.CONTENT_TYPE, AbstractZipStreamingResponseBody.MEDIA_TYPE)
                                 .header(HttpHeaders.CONTENT_DISPOSITION, getAttachmentDisposition(model.getLabel(), "zip"))
                                 .body(new CatalogZipStreamingResponseBody(getSessionUser(), catalog.catBean, _archivePath));
        } catch (ServerException e) {
            log.error("An error occurred trying to retrieve the resource catalog for the model {}", model.getId(), e);
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    private final String            _archivePath;
    private final MlModelService _service;
}
