/*
 * ml-plugin: org.nrg.xnatx.plugins.ml.XnatMlPlugin
 * XNAT http://www.xnat.org
 * Copyright (c) 2005-2021, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.xnatx.plugins.ml;

import com.fasterxml.jackson.databind.Module;
import com.fasterxml.jackson.databind.module.SimpleAbstractTypeResolver;
import com.fasterxml.jackson.databind.module.SimpleModule;
import lombok.extern.slf4j.Slf4j;
import org.nrg.framework.annotations.XnatDataModel;
import org.nrg.framework.annotations.XnatPlugin;
import org.nrg.xdat.model.*;
import org.nrg.xdat.om.*;
import org.nrg.xnatx.plugins.ml.converters.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;

@XnatPlugin(value = "xnatMlPlugin", name = "XNAT Machine Learning Development Plugin",
            dataModels = {@XnatDataModel(value = MlTrainconfig.SCHEMA_ELEMENT_NAME,
                                         singular = "Training Configuration",
                                         plural = "Training Configurations",
                                         code = "TRC"),
                          @XnatDataModel(value = MlTrainsession.SCHEMA_ELEMENT_NAME,
                                         singular = "Training Run",
                                         plural = "Training Runs",
                                         code = "TRR"),
                          @XnatDataModel(value = MlModel.SCHEMA_ELEMENT_NAME,
                                         singular = "Model",
                                         plural = "Models",
                                         code = "MDL")},
            logConfigurationFile = "ml-plugin-logback.xml")
@ComponentScan({"org.nrg.xft.serializers",
                "org.nrg.xnatx.plugins.ml.preferences",
                "org.nrg.xnatx.plugins.ml.rest",
                "org.nrg.xnatx.plugins.ml.services.impl"})
@Slf4j
public class XnatMlPlugin {
    @Bean
    public Module xnatMlModule() {
        final SimpleModule module = new SimpleModule();

        final SimpleAbstractTypeResolver resolver = new SimpleAbstractTypeResolver();
        resolver.addMapping(MlParameterizedconfigI.class, MlParameterizedconfig.class);
        resolver.addMapping(MlTrainconfigI.class, MlTrainconfig.class);
        resolver.addMapping(MlTrainsessionI.class, MlTrainsession.class);
        module.setAbstractTypes(resolver);

        module.addDeserializer(MlParameterizedconfig.class, new ParameterizedConfigDeserializer());
        module.addDeserializer(MlTrainconfig.class, new TrainconfigDeserializer());
        module.addDeserializer(MlTrainsession.class, new TrainsessionDeserializer());
        module.addSerializer(MlParameterizedconfig.class, new ParameterizedConfigSerializer());
        module.addSerializer(MlTrainconfig.class, new TrainconfigSerializer());
        module.addSerializer(MlTrainsession.class, new TrainsessionSerializer());
        module.addSerializer(MlModel.class, new ModelSerializer());
        return module;
    }
}
