/*
 * ml-plugin: org.nrg.xnatx.plugins.ml.models.DataSetModel
 * XNAT http://www.xnat.org
 * Copyright (c) 2005-2021, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.xnatx.plugins.ml.models;

import io.swagger.annotations.ApiModel;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import lombok.Data;
import lombok.experimental.Accessors;

@SuppressWarnings("unused")
@ApiModel(description = "Provides a container for ML data sets.")
@Data
@Accessors(prefix = "_")
public class DataSetModel {
    public int getNumTraining() {
        return _training.size();
    }

    public int getNumTest() {
        return _test.size();
    }

    private       String                  _name;
    private       String                  _description;
    private       String                  _reference;
    private       String                  _licence;
    private       String                  _release;
    private       String                  _tensorImageSize;
    private final Map<String, String>     _modality   = new HashMap<>();
    private final Map<String, String>     _labels     = new HashMap<>();
    private final List<DataSetExperiment> _training   = new ArrayList<>();
    private final List<DataSetExperiment> _validation = new ArrayList<>();
    private final List<String>            _test       = new ArrayList<>();
}
