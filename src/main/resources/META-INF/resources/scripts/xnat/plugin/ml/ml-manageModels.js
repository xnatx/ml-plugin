/*
 * ml-plugin: ml-manageModels.js
 * XNAT http://www.xnat.org
 * Copyright (c) 2005-2021, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

console.log('ml-manageModels.js');

var XNAT = getObject(XNAT || {});
XNAT.plugin = getObject(XNAT.plugin || {});
XNAT.plugin.ml = getObject(XNAT.plugin.ml || {});

(function(factory){
    if (typeof define === 'function' && define.amd) {
        define(factory);
    }
    else if (typeof exports === 'object') {
        module.exports = factory();
    }
    else {
        return factory();
    }
}(function() {

    /* ================ *
     * GLOBAL FUNCTIONS *
     * ================ */

    var undef;
    var projectId = XNAT.data.context.project || getQueryStringValue('project') || getQueryStringValue('id');
    var rootUrl   = XNAT.url.rootUrl;
    var restUrl   = XNAT.url.restUrl;
    var csrfUrl   = XNAT.url.csrfUrl;
    var xml2json  = new X2JS();

    function spacer(width) {
        return spawn('i.spacer', {
            style: {
                display: 'inline-block',
                width: width + 'px'
            }
        })
    }

    function unCamelCase(string){
        string = string.replace(/([A-Z])/g, " $1"); // insert a space before all capital letters
        return string.charAt(0).toUpperCase() + string.slice(1); // capitalize the first letter to title case the string
    }

    function errorHandler(e, title, closeAll) {
        console.log(e);
        title = (title) ? 'Error: ' + title : 'Error';
        closeAll = (closeAll === undef) ? true : closeAll;
        var errormsg = (e.statusText) ? '<p><strong>Error ' + e.status + ': ' + e.statusText + '</strong></p><p>' + e.responseText + '</p>' : e;
        XNAT.dialog.open({
            width: 450,
            title: title,
            content: errormsg,
            buttons: [
                {
                    label: 'OK',
                    isDefault: true,
                    close: true,
                    action: function () {
                        if (closeAll) {
                            xmodal.closeAll();

                        }
                    }
                }
            ]
        });
    }

    /* ======================== *
     * DISPLAY INSTALLED MODELS*
     * ======================== */

    var projectModels;

    XNAT.plugin.ml.projectModels = projectModels =
        getObject(XNAT.plugin.ml.projectModels || {});

    projectModels.installedModels = [], projectModels.modelNames = [];

    var getModelsUrl = function(){
        return rootUrl("/xapi/ml/models/"+projectId);
    };
    var getModelResourcesUrl = function(id,label){
        // return rootUrl("/data/experiments/"+id+"/resources/"+label+"/files?format=json")
        return rootUrl("/data/experiments/"+id+"/resources/Model/files?format=json")
    };

    function launchOnThisModel(model){
        var presets = JSON.stringify({ "model": model.id });
        return spawn('button.btn.btn-sm.primary.launchModel', {
            onclick: function(e){
                e.preventDefault();
                XNAT.plugin.ml.trainingRun.openDialog(presets);
            }
        }, 'Launch Training');
    }
    function viewModelReportButton(model){
        var id = model.id;
        return spawn('button.btn.btn-sm.viewModel', {
            onclick: function(e){
                e.preventDefault();
                window.location.assign(rootUrl('/data/experiments/'+id+'?format=html'));
            }
        }, 'View Model');
    }
    function publishModelButton(model){
        // does nothing for now
        return spawn('button.btn.btn-sm.disabled',{
            onclick: function(e){
                e.preventDefault();
            }
        }, [ spawn('i.fa.fa-cloud-upload', {title: 'Upload Model to Cloud'}) ]);
    }

    function deleteModelButton(model){
        return spawn('button.btn.btn-sm',{
            onclick: function(e){
                e.preventDefault();
                XNAT.plugin.ml.projectModels.deleteModel(model)
            }
        },'Delete Model')
    }
    function exportModelButton(model){
        return spawn('button.btn.btn-sm.export-model-button.disabled',{
            id: 'export-'+model.id,
            onclick: function(e){
                e.preventDefault();
                XNAT.plugin.ml.projectModels.exportModel(model)
            }
        },'Export Model')
    }

    function showModelCounts(){
        showCounts("model")
    }

    projectModels.exportModel = function(model){
        XNAT.ui.dialog.message({
            title: 'Export Model',
            content: 'To export the desired state of a model, please select a completed training run and click the "Download" button to get a package of the training outputs.'
        })
    };

    projectModels.deleteModel = function(model){
        XNAT.ui.dialog.message({
            title: 'Confirm Delete',
            content: 'Are you sure you want to delete the model named '+model.label+'? Any training runs associated with this model will still be stored in XNAT, but cannot be viewed in this dashboard.',
            buttons: [
                {
                    label: 'Delete Model',
                    isDefault: true,
                    close: false,
                    action: function(){
                        xmodal.loading.open({title: 'Deleting Model...'});
                        XNAT.xhr.ajax({
                            url: csrfUrl('/xapi/ml/models/model/'+model.id),
                            method: 'DELETE',
                            success: function(){
                                xmodal.loading.close();
                                XNAT.ui.dialog.closeAll();
                                XNAT.ui.banner.top(3000,'Model successfully deleted','success');
                                XNAT.plugin.ml.projectModels.init('refresh')
                            },
                            fail: function(e){
                                xmodal.loading.close();
                                errorHandler(e,'Could not delete model',true);
                            }
                        })
                    }
                }
            ]
        })
    };

    projectModels.buildList = function(container, callback){
        XNAT.xhr.getJSON({
            url: getModelsUrl(),
            fail: function (e) {
                errorHandler(e, "Could not retrieve models for " + projectId)
            },
            success: function (data) {
                // empty array if it was previously populated
                projectModels.installedModels = [];
                projectModels.modelNames = [];

                var modelIndex = Object.keys(data);
                modelIndex.forEach(function (k) {
                    projectModels.installedModels.push({"id": k, "label": data[k], "files": [], "training": []});
                    projectModels.modelNames.push(data[k]);
                });
                projectModels.installedModels.sort(function(a,b){ return (a.id > b.id)? -1: 1 });

                if (projectModels.installedModels.length){
                    container.empty();
                    projectModels.installedModels.forEach(function(model){
                        container.append(spawn(null,[
                            spawn('div.data-table-container.model-container',{ data: { 'modelid': model.id }}, [
                                spawn('div.data-table-titlerow',[
                                    spawn('h3.data-table-title', model.label ),
                                    spawn('div.data-table-actionsrow',[
                                        viewModelReportButton(model),
                                        spacer(6),
                                        exportModelButton(model),
                                        spacer(6),
                                        deleteModelButton(model),
                                        spacer(6),
                                        spawn('span.pull-right',[
                                            launchOnThisModel(model)
                                        ])
                                    ])
                                ]),
                                spawn('div.data-table-wrapper',[
                                    spawn('div.modelTrainingList',{ id: model.id+'-training', html: 'No training runs yet.' })
                                ])
                            ]),
                            spawn('div.clearfix.clear',{style: {'margin':'1em 0'}})
                        ]))
                    });
                    showModelCounts();
                }
            }
        });

        if (isFunction(callback)) callback();
    };

    projectModels.init = projectModels.refresh = function(refresh){
        refresh = refresh || false;
        var $modelList = $('div#proj-model-list-container');

        // attempt to get prefs, then build model list
        XNAT.xhr.getJSON({
            url: restUrl('/xapi/ml/prefs'),
            success: function(data){
                projectModels.prefs = data;
                projectModels.buildList($modelList, projectTrainingRuns.init);
            },
            fail: function(e){
                console.log('Could not retrieve XNAT ML preferences');
                console.warn(e);
                projectModels.prefs = {  tensorBoardEnabled: 'false' }; // default
                projectModels.buildList($modelList, projectTrainingRuns.init);
            }
        });

        if (!refresh){
            // add help button to header
            var $header = $modelList.parents('.panel').find('.panel-heading');
            var helpButton = spawn('button.btn.btn-hover.sm.pull-right', {
                style: {color: '#fff'},
                onclick: function(e){
                    e.preventDefault();

                    XNAT.dialog.message({
                        width: 550,
                        title: 'Help Installing and Managing Models',
                        content: spawn('!',[
                            spawn('p','XNAT ML functionality supports a limited number of model formats. Model files should be compressed into a single archive and uploaded to XNAT via the UI.'),
                            spawn('p','For help installing models or managing them, see <a href="https://wiki.xnat.org/ml/installing-a-model" target="_blank"><b>XNAT ML Documentation</b></a>')
                        ])
                    })
                }
            },[ spawn('i.fa.fa-info-circle') ]);
            $header.prepend(helpButton);

            // add functional buttons to footer
            var $footer = $modelList.parents('.panel').find('.panel-footer');
            var newModel = spawn('button.new-model.btn.btn-sm.submit', {
                html: 'Install New Model',
                onclick: function(e){
                    e.preventDefault();

                    // form with file input to render
                    var modelNameInput = spawn('input|type=text|name=model-name|size=30|placeholder=Model Name|required');

                    var fileForm$ = $.spawn('form#model-upload', {
                        style: { padding: '10px', fontSize: '13px' }
                    }, [
                        ['p', ['Training Model Name: ', modelNameInput]],
                        ['p', 'Select a Training Model to upload:'],
                        ['br'],
                        ['input|type=file|name=modelFile|accept=.zip,.tgz,.tar.gz|required']
                    ]);

                    var fileForm = fileForm$[0];

                    XNAT.dialog.open({
                        title: 'Upload Training Model',
                        width: 320,
                        content: fileForm,
                        beforeShow: function(obj){
                            modelNameInput.classList.remove('error');
                            modelNameInput.classList.remove('invalid');
                        },
                        okLabel: 'Upload',
                        okClose: false,
                        okAction: function(obj){
                            var modelName = modelNameInput.value.replace(/([!@#$%^&*\'\"\[\]]|\-|\s)+/g,'_'); // replace all spaces and special chars
                            if (!modelName) {
                                XNAT.dialog.message('Error', 'The Model Name is required');
                                modelNameInput.classList.add('error');
                                modelNameInput.classList.add('invalid');
                                return false;
                            }

                            if (XNAT.plugin.ml.projectModels.modelNames.indexOf(modelName) >= 0) {
                                // prevent saving a model with a duplicate name
                                XNAT.dialog.message('Error', 'The Model Name cannot duplicate an existing model in this '+XNAT.app.displayNames.singular.project.toLowerCase()+'.');
                                modelNameInput.classList.add('error');
                                modelNameInput.classList.add('invalid');
                                return false;
                            }

                            var modelFile = obj.$dialog.find('input[name=modelFile]');
                            if (!modelFile.val()) {
                                XNAT.dialog.message('Error', 'A Model File is required');
                                modelFile.classList.add('error');
                                modelFile.classList.add('invalid');
                                return false;
                            }
                            xmodal.loading.open('Installing Model...');

                            var postModel = XNAT.xhr.post({
                                url: rootUrl('/xapi/ml/models/model/' + projectId + '/' + modelName),
                                data: (new FormData(fileForm)),
                                cache: false,
                                contentType: false,
                                processData: false,
                                dataType: 'text', // what's the RESPONSE?
                                success: function(data){
                                    console.log(data);
                                    var results = [];
                                    xmodal.loading.close();
                                    obj.close();
                                    XNAT.ui.banner.top(3000, 'Training Model uploaded.', 'success');
                                    XNAT.plugin.ml.projectModels.init('refresh');
                                },
                                failure: function(){
                                    xmodal.loading.close();
                                    console.warn('error...');
                                    console.warn(arguments);
                                    XNAT.dialog.message('Error', 'An error occurred uploading the Training Model.');
                                }
                            });
                        }
                    });

                }
            });

            // add the 'add new' button to the panel footer
            $footer.append(spawn('div.pull-right', [
                newModel
            ]));
            $footer.append(spawn('div.clear.clearFix'));
        }
    };

    /* ================================ *
     * DISPLAY TRAINING SESSIONS / RUNS *
     * ================================ */

    var projectTrainingRuns;

    XNAT.plugin.ml.projectTrainingRuns = projectTrainingRuns =
        getObject(XNAT.plugin.ml.projectTrainingRuns || {});

    projectTrainingRuns.list = [];

    var getTrainingRunsUrl = function(){
        return rootUrl("/xapi/ml/train/project/"+projectId);
    };
    var getTrainingRunUrl = function(id){
        return rootUrl("/xapi/ml/train/project/"+projectId+"/"+id);
    };
    var liveTrainingViewUrl = function(id){
        return window.location.origin + rootUrl('/training/'+id+'-live'+'/')
    };
    var archivedTrainingViewUrl = function(id){
        return window.location.origin + rootUrl('/training/'+id+'/')
    };

    function viewTrainingExperimentLink(run){
        return spawn('a',{ href: rootUrl('/data/experiments/'+run.id+'?format=html') },run.label );
    }
    function viewTrainingDatasetLink(run){
        if (run.collectionId) {
            return spawn('a.view-dataset',{ href: 'javascript:void', data: { id: run.collectionId }},'View Dataset');
        }
    }
    function viewTrainingDatasetButton(run){
        if (run.collectionId) {
            return spawn('button.view-dataset.btn.btn-sm',{ href: 'javascript:void', data: { id: run.collectionId }, title: 'View Dataset'},[
                spawn('i.fa.fa-table')
            ]);
        }
    }
    function viewLiveTensorBoardButton(sessionId){
        return spawn('button.btn.btn-sm.view-training-board-button',{
            style: { color: '#393' },
            onclick: function(){
                xmodal.loading.open({title: "Launching Live TensorBoard Container"});

                window.setTimeout(function(){

                    // check for a live running container.
                    XNAT.xhr.ajax({
                        url: liveTrainingViewUrl(sessionId),
                        method: 'GET',
                        fail: function(e){
                            xmodal.loading.close();
                            XNAT.dialog.message({
                                title: "TensorBoard Launch Error",
                                content: "Could not open a Live TensorBoard View for this training run. Perhaps it has already completed?"
                            });
                            console.log("TensorBoard Launch Error",e);
                        },
                        success: function(){
                            xmodal.loading.close();
                            XNAT.ui.dialog.iframe(
                                liveTrainingViewUrl(sessionId),
                                'View Live TensorBoard for Training Run',
                                800,
                                '85%',
                                {
                                    footer: {
                                        content: "If content does not load successfully, try refreshing the window."
                                    },
                                    buttons: {
                                        close: {
                                            label: 'Close',
                                            isDefault: true,
                                            close: true
                                        },
                                        refresh: {
                                            label: 'Refresh',
                                            isDefault: false,
                                            action: function(obj){
                                                obj.$modal.find('iframe').prop('src',liveTrainingViewUrl(sessionId));
                                            }
                                        }
                                    }
                                }
                            )
                        }
                    })
                }, 2000);
            }
        }, [
            spawn('i.fa.fa-bar-chart',{ title: 'View Live Tensor Board on Training In Progress' })
        ])
    }

    function viewTensorBoardButton(run){
        if (!run.status) {
            console.log('Cannot place Tensorboard Button, run has no status');
            return;
        }
        var sessionId = run.id;
        if (run.status.toLowerCase() === "running") {
            return spawn('!',[viewLiveTensorBoardButton(sessionId), spacer(6)]);
        } else if (run.status.toLowerCase() === "complete") {
            return spawn('!', [spawn('button.btn.btn-sm.view-training-board-button',{
                onclick: function(){
                    projectTrainingRuns.viewTensorBoard(sessionId);
                }
            }, [
                spawn('i.fa.fa-bar-chart',{ title: 'View Archived Tensor Board' })
            ]), spacer(6)]);
        } else {
            console.log('Cannot place TensorBoard button, run status: '+run.status);
        }
    }

    function downloadResultsButton(id,resourceName){
        return spawn('button.btn.btn-sm.download-training-run-button',{
            data: { "runid":id },
            onclick: function(){
                window.location.assign(rootUrl('/data/experiments/'+id+'/resources/'+resourceName+'/files?format=zip'));
            }
        }, [
            spawn('i.fa.fa-download',{ title: 'Download Results' })
        ]);
    }
    function promoteModelButton(modelId,sessionId){
        return spawn('button.btn.btn-sm.promote-model-button',{
            data: { 'sessionid': sessionId, 'modelid': modelId },
            onclick: function(){
                // XNAT.plugin.ml.projectTrainingRuns.verifyTrainingFiles(id,projectTrainingRuns.promoteModel)
                XNAT.plugin.ml.projectTrainingRuns.promoteModel(modelId,sessionId)
            }
        },[
            spawn('i.fa.fa-hand-o-up',{ title: 'Promote Training Results to New Model' })
        ])
    }
    projectTrainingRuns.viewTensorBoard = function(sessionId){
        // check for the existence of the tensorboard command
        var tbCommands = XNAT.plugin.ml.trainingRun.commands
                             .filter(function(command){ return command['wrapper-name'] === "tensorboard-session" });

        if (!tbCommands.length) {
            XNAT.dialog.message('This '+XNAT.app.displayNames.singular.project.toLowerCase()+ ' needs the "tensorboard-session" command enabled.');
            return false;
        }
        else {
            var command = tbCommands[0];
            xmodal.loading.open({title: "Launching TensorBoard Container"});

            var launchData = {session: sessionId};
            launchData['xnat:trainSession'] = '/archive/experiments/' + sessionId;
            XNAT.xhr.ajax({
                url: csrfUrl('/xapi/wrappers/'+command['wrapper-id']+'/root/xnat:trainSession/launch'),
                data: launchData,
                method: 'POST',
                fail: function(e){
                    xmodal.loading.close();
                    XNAT.dialog.message({
                        title: "TensorBoard Launch Error",
                        content: "Could not open TensorBoard for this training run. Do you already have a TensorBoard container running? If so, please stop that container and try this command again."
                    });
                    console.log("TensorBoard Launch Error",e);
                },
                success: function(data){
                    xmodal.loading.close();
                    xmodal.loading.open({title: 'Waiting for TensorBoard launch...'});
                    waitForWorkflow(data['workflow-id'], function(){
                        openTensorboardiFrame(sessionId, projectId);
                    });
                }
            })
        }
    }

    function openTensorboardiFrame(sessionId, projectId) {
        XNAT.xhr.ajax({
            url: archivedTrainingViewUrl(sessionId),
            method: 'HEAD',
            success: function() {
                xmodal.loading.close();
                XNAT.ui.dialog.iframe(
                    archivedTrainingViewUrl(sessionId),
                    'View TensorBoard for Training Run',
                    800,
                    '85%',
                    {
                        onClose: function () {
                            // kill the container that runs the TensorBoard view
                            XNAT.xhr.ajax({
                                url: csrfUrl('/xapi/projects/'+projectId+'/containers/' + sessionId + '/kill'),
                                method: 'POST',
                                fail: function (e) {
                                    errorHandler(e, "Could not kill TensorBoard container")
                                },
                                success: function () {
                                    XNAT.ui.dialog.closeAll();
                                    console.log('TensorBoard container killed', sessionId);
                                }
                            })
                        },
                        footer: {
                            content: "If content does not load successfully, try refreshing the window."
                        },
                        buttons: {
                            close: {
                                label: 'Close',
                                isDefault: true,
                                close: true
                            },
                            refresh: {
                                label: 'Refresh',
                                isDefault: false,
                                action: function(obj){
                                    obj.$modal.find('iframe').prop('src',archivedTrainingViewUrl(sessionId));
                                }
                            }
                        }
                    }
                )
            },
            error: function(e) {
                if (e.status === 404) {
                    // wait
                    window.setTimeout(function() {
                        openTensorboardiFrame(sessionId, projectId);
                    }, 1000);
                } else {
                    errorHandler(e, "Could not open TensorBoard");
                }
            }
        });
    }

    function waitForWorkflow(workflowId, callback) {
        //NOTE: this does not close any loading modals for you
        XNAT.xhr.getJSON({
            url: XNAT.url.restUrl('/xapi/workflows/'+workflowId),
            success: function(data) {
                if (data['status'].toLowerCase() === 'running') {
                    callback();
                } else if (data['status'].toLowerCase().includes('failed')) {
                    xmodal.loading.close();
                    XNAT.ui.dialog.alert('Error launching TensorBoard. ' + data['status'] + ': ' + data['details']);
                } else {
                    window.setTimeout(function(){
                        waitForWorkflow(workflowId, callback);
                    }, 1000);
                }
            },
            error: function(xhr) {
                xmodal.loading.close();
                XNAT.ui.dialog.alert('Error getting status for workflow: ' + xhr.status + ' ' + xhr.responseText);
            }
        });
    }

    // create data table
    projectTrainingRuns.table = function(container, model, callback) {

        // initialize the table - we'll add to it below
        var ptrTable = XNAT.table({
            className: 'project-training-runs xnat-table',
            style: { width: '100%' }
        });

        // add table header row
        ptrTable.tr()
            .th({addClass: 'left', html: '<b>Training Run</b>'})
            .th('<b>Parameters</b>')
            // .th('<b>Dataset</b>')
            .th('<b>Launch Date</b>')
            .th('<b>Status</b>')
            .th('<b>Actions</b>');

        var modelTrainingList = model.training.sort(function(a,b){ return (a.launchTime < b.launchTime) ? 1 : -1 });

        if(modelTrainingList.length){
            // enable "export" button
            $('#export-'+model.id).removeClass('disabled');

            modelTrainingList.forEach(function(run){
                var actions = [
                    viewTrainingDatasetButton(run),
                ];
                if (projectModels.prefs.tensorBoardEnabled.toString() === 'true') actions.push(viewTensorBoardButton(run));

                if (run.hasResults === "true") {
                    actions.push(spacer(6),
                        downloadResultsButton(run.id,'Results'),
                        spacer(6),
                        promoteModelButton(run.modelId, run.id))
                }
                ptrTable.tr({ id: run.id, data: {"training-run" : run.label }})
                    .td({ style: { 'word-break':'break-all', 'max-width':'360px', 'font-weight':'bold'}},viewTrainingExperimentLink(run))
                    .td([[ 'div.center.training-run-params', [ objToArray(JSON.parse(run.parameters)).join('<br/>') ]]])
                    // .td({ addClass: 'training-run-dataset'}, viewTrainingDatasetLink(run))
                    .td([[ 'div.center.training-run-date', [ new Date(run.launchTime).toLocaleString() ]]])
                    .td([[ 'div.center.training-run-status', [ run.status ]]])
                    .td([[ 'div.training-run-actions', actions]])
            });
        }
        else {
            ptrTable.tr()
                    .td({ colspan: 5, html: 'No training runs have been launched on this model.'});
        }
        if (container){
            $$(container).empty();
            $$(container).append(ptrTable.table);
        }

        if (isFunction(callback)) {
            callback(ptrTable.table);
        }

    };

    projectTrainingRuns.viewTrainingRunXml = function(id, opts){
        // var readonly = (opts) ? opts.readonly : false;
        var readonly = true;
        XNAT.xhr.ajax({
            method: 'GET',
            contentType: 'application/xml',
            url: getTrainingRunUrl(id),
            fail: function(e){ errorHandler(e,"Could not retrieve XML for "+id)},
            success: function(data){
                var xmlData = new XMLSerializer().serializeToString(data);
                _source = spawn ('textarea', xmlData);

                _editor = XNAT.app.codeEditor.init(_source, {
                    language: 'xml'
                });

                _editor.openEditor({
                    title: 'View Training Run Experiment: ' + opts.label,
                    classes: 'plugin-xml',
                    buttons: {
                        ok: {
                            label: 'OK',
                            isDefault: true,
                            close: true
                        },
                        cancel: {
                            label: 'Cancel',
                            close: true
                        }
                    },
                    height: 680,
                    afterShow: function(dialog, obj){
                        obj.aceEditor.setReadOnly(readonly);
                    }
                });
            }
        })
    };

    projectTrainingRuns.verifyTrainingFiles = function(id,callback){
        // verifies that this training run has resource files that could be used in future training or publishing

        XNAT.xhr.get({
            url: rootUrl('/data/experiments/'+id+'/resources/Results'),
            fail: function(e){ errorHandler(e,"Could not retrieve Results resource for "+id)},
            success: function(xmlData){
                var jsonData = xml2json.xml2json(xmlData);
                try {
                    var resourceFilesFound = jsonData.Catalog.entries.entry;
                    if (resourceFilesFound.length) {
                        if (isFunction(callback)) callback(id,resourceFilesFound);
                    }
                    else {
                        errorHandler({
                            status: '400',
                            statusText: 'No Resources Found',
                            responseText: 'Cannot proceed, no results files were found in this Training Run.'
                        });
                    }
                }
                catch(e){
                    errorHandler(e,"Could not retrieve resource files, process cannot continue.");
                    console.log("verifyTrainingFiles:", e);
                }

            }
        })
    };

    projectTrainingRuns.promoteModel = function(modelId,sessionId){
        // open a modal that allows user to set parameters for model promotion / publishing
        var model = projectModels.installedModels.filter(function(model){ return model.id === modelId})[0];

        XNAT.dialog.open({
            title: 'Convert Training Results To Model',
            width: 600,
            content: spawn('form.promoteTrainingResults', [spawn('div.panel')]),
            beforeShow: function (obj) {
                var inputArea = obj.$modal.find('.promoteTrainingResults').find('.panel');
                inputArea.append(spawn('!',[
                    spawn('div.message',{style: {'margin-bottom':'1em'}},'This function allows you to take the resulting files from a Training Run and promote them to a new model. This algorithm takes the "Final" version of the checkpoint file.'),
                    XNAT.ui.input.hidden({
                        name: 'sessionId',
                        value: sessionId
                    }).spawned,
                    XNAT.ui.input.hidden({
                        name: 'project',
                        value: projectId
                    }).spawned,
                    XNAT.ui.panel.input({
                        name: 'label',
                        label: 'Model Name',
                        value: model.label,
                        size: 50,
                        data: { 'modelname': model.label }
                    }),
                    XNAT.ui.panel.element({
                        label: "Select Model",
                        html: spawn('select',{name: 'useBestModel', addClass: 'best-model-selector'}, [
                            spawn('option',{value: 'false', selected: true }, 'Use Final Model'),
                            spawn('option',{value: 'true'}, 'Use Best Model')
                        ])
                    }).spawned
                ]));
            },
            afterShow: function(obj){
                var inputArea = obj.$modal.find('.promoteTrainingResults').find('.panel');
            },
            buttons: [
                {
                    label: 'OK',
                    isDefault: true,
                    close: false,
                    action: function(obj){
                        // pull form values
                        var formInputs = convertFormToObject(obj.$modal.find('form').serializeArray());

                        // ensure that model names are not duplicated
                        // validate()
                        if  (XNAT.plugin.ml.projectModels.installedModels.filter(function(model){ return model.label === formInputs.label }).length) {
                            obj.$modal.find('input[name=label]').addClass('invalid');
                            XNAT.dialog.message({
                                title: 'Error',
                                content: 'Please specify a unique model name'
                            });
                            return false;
                        }

                        xmodal.loading.open({
                            title: 'Preparing to convert training results'
                        });

                        XNAT.xhr.ajax({
                            method: 'POST',
                            url: csrfUrl('/xapi/ml/train/'+formInputs.sessionId+'/createModel/'+formInputs.project+'/'+formInputs.label)+'&useBestModel='+formInputs.useBestModel,
                            fail: function(e){
                                xmodal.loading.close();
                                errorHandler(e,"Could Not Create New Model")
                            },
                            success: function(data){
                                xmodal.loading.close();
                                XNAT.ui.dialog.closeAll();
                                XNAT.ui.dialog.message({
                                    title: "Process Complete",
                                    content: "New model created: "+data,
                                    buttons: [
                                        {
                                            label: 'OK',
                                            isDefault: true,
                                            close: true
                                        }
                                    ]
                                });
                                XNAT.plugin.ml.projectModels.refresh(true);
                            }
                        })
                    }
                },
                {
                    label: 'Cancel',
                    isDefault: false,
                    close: true
                }
            ]
        })
    };

    function convertFormToObject(formInputs){
        var formData = {};
        formInputs.forEach(function(input){
            formData[input.name] = input.value
        });
        return formData;
    }

    function objToArray(obj){
        var array = [], keys = Object.keys(obj);
        keys.forEach(function(key){ array.push(key + ": "+obj[key]) });
        return array;
    }

    projectTrainingRuns.init = projectTrainingRuns.refresh = function(){

        // get training runs for this project, then sort by model.
        XNAT.xhr.getJSON({
            url: getTrainingRunsUrl(),
            fail: function (e) {
                errorHandler(e, "Could not retrieve training runs for " + projectId)
            },
            success: function (data) {
                // reset and rebuild cached list
                projectTrainingRuns.list = [];

                var sessionIndex = Object.keys(data);
                sessionIndex.forEach(function (k) {
                    // don't store training runs that are orphaned from deleted models
                    if (typeof data[k].modelId !== "undefined") projectTrainingRuns.list.push( data[k] );
                });

                projectModels.installedModels.forEach(function(model){
                    // find the matching model entry for each training run, if they exist, and add this training run to that model's list of runs.
                    var modelRuns = projectTrainingRuns.list.filter(function(run){ return run.modelId === model.id });
                    if (modelRuns.length) model.training = modelRuns;
                });

                // iterate over every model and render a table of training runs for each
                projectModels.installedModels.forEach(function(model){
                    var $modelTrainingList = $(document).find('#'+model.id+'-training');
                    projectTrainingRuns.table($modelTrainingList, model, function() {
                        showCounts("runs");
                    });
                });

            }
        });

    };

    function showCounts(item) {
        var cssClass = "." + item + "-count";
        $(document).find(cssClass).empty();
        switch (item) {
            case "model":
                if (projectModels.installedModels.length) {
                    $(document).find(cssClass).append(" (" + projectModels.installedModels.length + ")")
                }
                break;
            case "config":
                if (projectTrainingConfigs.installedConfigs.length) {
                    $(document).find(cssClass).append(" (" + projectTrainingConfigs.installedConfigs.length + ")")
                }
                break;
            case "runs":
                if (projectTrainingRuns.list.length) {
                    $(document).find(cssClass).append(" (" + projectTrainingRuns.list.length + ")")
                }
                break;
            default:
                console.log("Error: can't show counts for unrecognized object " + item);
                break;
        }
    }

    try {
        projectModels.init();
    } catch(e) {
        errorHandler(e);
    }

}));